-- MySQL dump 10.16  Distrib 10.2.8-MariaDB, for osx10.12 (x86_64)
--
-- Host: localhost    Database: pips
-- ------------------------------------------------------
-- Server version	10.2.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actual_trade`
--

DROP TABLE IF EXISTS `actual_trade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actual_trade` (
  `id_actual_trade` bigint(20) NOT NULL AUTO_INCREMENT,
  `currency` varchar(255) DEFAULT NULL,
  `foreign_exchange_rate` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `staff_code` varchar(255) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `value_in_sgd` double DEFAULT NULL,
  `fk_broker` bigint(20) DEFAULT NULL,
  `fk_counter` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_actual_trade`),
  KEY `FKfeyquuerg3isdu7a9nrlc2bom` (`fk_broker`),
  KEY `FK4dg1s5wqjh7xyn4ni7jm9rf5r` (`fk_counter`),
  KEY `FK7tpnroh2nsyr3ppxv07re05hi` (`fk_user`),
  CONSTRAINT `FK4dg1s5wqjh7xyn4ni7jm9rf5r` FOREIGN KEY (`fk_counter`) REFERENCES `counter` (`id_counter`),
  CONSTRAINT `FK7tpnroh2nsyr3ppxv07re05hi` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKfeyquuerg3isdu7a9nrlc2bom` FOREIGN KEY (`fk_broker`) REFERENCES `broker` (`id_broker`)
) ENGINE=InnoDB AUTO_INCREMENT=810 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `api_token`
--

DROP TABLE IF EXISTS `api_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_token` (
  `id_api_token` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) DEFAULT NULL,
  `bank_id` varchar(255) DEFAULT NULL,
  `expiry_date_time` datetime DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `id_application` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `am_in_financial_position` int(11) NOT NULL,
  `do_not_posses_not_aware_of` int(11) NOT NULL,
  `for_sell_transaction` int(11) NOT NULL,
  `securities_not_on_blackout_list` int(11) NOT NULL,
  `there_is_no_conflict_of_intrest` int(11) NOT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_application`),
  KEY `FK4ofyd6rnxslha0mrkavgu4bvt` (`fk_user_created`),
  KEY `FKbl76kj0yrnk0i31t5fbxjbnt4` (`fk_user_last_modified`),
  CONSTRAINT `FK4ofyd6rnxslha0mrkavgu4bvt` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKbl76kj0yrnk0i31t5fbxjbnt4` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audit_log`
--

DROP TABLE IF EXISTS `audit_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_log` (
  `id_audit_log` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `fk_user_id` bigint(20) DEFAULT NULL,
  `log_message` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `revision` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_audit_log`)
) ENGINE=InnoDB AUTO_INCREMENT=4973 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audit_log_blackoutlist`
--

DROP TABLE IF EXISTS `audit_log_blackoutlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_log_blackoutlist` (
  `id_audit_log_blackoutlist` bigint(20) NOT NULL,
  `fk_audit_log` bigint(20) NOT NULL,
  `fk_blackout_list` bigint(20) NOT NULL,
  PRIMARY KEY (`fk_blackout_list`,`fk_audit_log`),
  KEY `FKmrgku7b6g2140flhu74k9wsn0` (`fk_audit_log`),
  CONSTRAINT `FK58lm43m4kc8le3ra089kul7m7` FOREIGN KEY (`fk_blackout_list`) REFERENCES `blackout_list` (`id_blackout_list`),
  CONSTRAINT `FKmrgku7b6g2140flhu74k9wsn0` FOREIGN KEY (`fk_audit_log`) REFERENCES `audit_log` (`id_audit_log`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audit_log_greylist`
--

DROP TABLE IF EXISTS `audit_log_greylist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_log_greylist` (
  `id_audit_log_greylist` bigint(20) NOT NULL AUTO_INCREMENT,
  `fk_grey_list` bigint(20) NOT NULL,
  `fk_grey_list_copy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_audit_log_greylist`),
  KEY `FK1nl8btnfjvhu46m5nxhlqjqa4` (`fk_grey_list`),
  KEY `FK4kinp3bjias9igir4o3tk9piu` (`fk_grey_list_copy`),
  CONSTRAINT `FK1nl8btnfjvhu46m5nxhlqjqa4` FOREIGN KEY (`fk_grey_list`) REFERENCES `grey_list` (`id_grey_list`),
  CONSTRAINT `FK4kinp3bjias9igir4o3tk9piu` FOREIGN KEY (`fk_grey_list_copy`) REFERENCES `grey_list_copy` (`id_grey_list_copy`)
) ENGINE=InnoDB AUTO_INCREMENT=7915 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `authorized_personnal_copy`
--

DROP TABLE IF EXISTS `authorized_personnal_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorized_personnal_copy` (
  `id_authorized_personnal_copy` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `authorized_personnal_ids` text DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_grey_list_copy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_authorized_personnal_copy`),
  KEY `FKcqeoktyick8nv71k3j8ggar0f` (`fk_user_created`),
  KEY `FK705fwgyownpebb074dettf1h2` (`fk_user_last_modified`),
  KEY `FKtfxi27lxxm2uydbu84840aen2` (`fk_grey_list_copy`),
  CONSTRAINT `FK705fwgyownpebb074dettf1h2` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKcqeoktyick8nv71k3j8ggar0f` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKtfxi27lxxm2uydbu84840aen2` FOREIGN KEY (`fk_grey_list_copy`) REFERENCES `grey_list_copy` (`id_grey_list_copy`)
) ENGINE=InnoDB AUTO_INCREMENT=4049 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blackout_list`
--

DROP TABLE IF EXISTS `blackout_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blackout_list` (
  `id_blackout_list` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `additional_comments` text DEFAULT NULL,
  `client_solicitation_end_date` datetime DEFAULT NULL,
  `client_solicitation_start_date` datetime DEFAULT NULL,
  `personal_investment_policy_end_date` datetime DEFAULT NULL,
  `personal_investment_policy_start_date` datetime DEFAULT NULL,
  `proprietary_trading_end_date` datetime DEFAULT NULL,
  `proprietary_trading_start_date` datetime DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `research_end_date` datetime DEFAULT NULL,
  `research_start_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_counter` bigint(20) DEFAULT NULL,
  `revision_no` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_blackout_list`),
  KEY `FK_8qstij924vcap7vwncgr3ypro` (`fk_user_created`),
  KEY `FK_7jhpv3l2h95bc2v92xr5vki79` (`fk_user_last_modified`),
  KEY `FK_j35qe45et1gotf8t1ev96mopt` (`fk_counter`),
  CONSTRAINT `FK_7jhpv3l2h95bc2v92xr5vki79` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_8qstij924vcap7vwncgr3ypro` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_j35qe45et1gotf8t1ev96mopt` FOREIGN KEY (`fk_counter`) REFERENCES `counter` (`id_counter`),
  CONSTRAINT `FKbxwp5v1lgrpu6kqf6f68v6ndm` FOREIGN KEY (`fk_counter`) REFERENCES `counter` (`id_counter`),
  CONSTRAINT `FKqxosos362d6nm7uys2mvhf5xm` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKrgi84y8ddlu9dvu63gluhn9he` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blackout_list_copy`
--

DROP TABLE IF EXISTS `blackout_list_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blackout_list_copy` (
  `id_blackout_list_copy` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `additional_comments` varchar(255) DEFAULT NULL,
  `client_solicitation_end_date` datetime DEFAULT NULL,
  `client_solicitation_start_date` datetime DEFAULT NULL,
  `personal_investment_policy_end_date` datetime DEFAULT NULL,
  `personal_investment_policy_start_date` datetime DEFAULT NULL,
  `proprietary_trading_end_date` datetime DEFAULT NULL,
  `proprietary_trading_start_date` datetime DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `research_end_date` datetime DEFAULT NULL,
  `research_start_date` datetime DEFAULT NULL,
  `revision_no` bigint(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_counter` bigint(20) DEFAULT NULL,
  `fk_blackout_list` bigint(20) DEFAULT NULL,
  `audit_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_blackout_list_copy`),
  KEY `FKnqnxn0xx10ivvdymcf42002ne` (`fk_user_created`),
  KEY `FKk100neomgmcraarq8bnusgbod` (`fk_user_last_modified`),
  KEY `FK43xlwc3mq52vvppbex7hscpdt` (`fk_counter`),
  KEY `FKihu3q8ad61bkgxf8uupgnu91u` (`fk_blackout_list`),
  CONSTRAINT `FK43xlwc3mq52vvppbex7hscpdt` FOREIGN KEY (`fk_counter`) REFERENCES `counter` (`id_counter`),
  CONSTRAINT `FKihu3q8ad61bkgxf8uupgnu91u` FOREIGN KEY (`fk_blackout_list`) REFERENCES `blackout_list` (`id_blackout_list`),
  CONSTRAINT `FKk100neomgmcraarq8bnusgbod` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKnqnxn0xx10ivvdymcf42002ne` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2642 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `breach`
--

DROP TABLE IF EXISTS `breach`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `breach` (
  `id_breach` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `is_breach` int(11) NOT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_breach_type` bigint(20) DEFAULT NULL,
  `fk_broker` bigint(20) DEFAULT NULL,
  `fk_counter` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_breach`),
  KEY `FKmtatv71876dfqqyyeeugbvf1` (`fk_user_created`),
  KEY `FKeuyndwnuik0u2g1yf7err0don` (`fk_user_last_modified`),
  KEY `FKnt7hcahvwkhsv9nhi449vp7r7` (`fk_breach_type`),
  KEY `FK1ltn2rkasd4ddm3dxjc8our2x` (`fk_broker`),
  KEY `FKgcf605a4rpuwrxyerwwvyavrt` (`fk_counter`),
  KEY `FKhndgggb555tviq63u1orxkk03` (`fk_user`),
  CONSTRAINT `FK1ltn2rkasd4ddm3dxjc8our2x` FOREIGN KEY (`fk_broker`) REFERENCES `broker` (`id_broker`),
  CONSTRAINT `FKeuyndwnuik0u2g1yf7err0don` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKgcf605a4rpuwrxyerwwvyavrt` FOREIGN KEY (`fk_counter`) REFERENCES `counter` (`id_counter`),
  CONSTRAINT `FKhndgggb555tviq63u1orxkk03` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKmtatv71876dfqqyyeeugbvf1` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKnt7hcahvwkhsv9nhi449vp7r7` FOREIGN KEY (`fk_breach_type`) REFERENCES `breach_type` (`id_breach_type`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `breach_applications`
--

DROP TABLE IF EXISTS `breach_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `breach_applications` (
  `id_breach_applications` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_breach` bigint(20) DEFAULT NULL,
  `fk_investment` bigint(20) DEFAULT NULL,
  `fk_breach_type` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_breach_applications`),
  KEY `FKpylsnb2nnya4bxtq3xd3kgmi2` (`fk_user_created`),
  KEY `FKbawdcdhpe6wv5970eyw6pi745` (`fk_user_last_modified`),
  KEY `FK60b1608fq6mln63eet00ybro6` (`fk_breach`),
  KEY `FKd82bqpoxg1fe7e53v45252wk7` (`fk_investment`),
  KEY `FK2sv7oj0fki9dwytfqfso2h28h` (`fk_breach_type`),
  CONSTRAINT `FK2sv7oj0fki9dwytfqfso2h28h` FOREIGN KEY (`fk_breach_type`) REFERENCES `breach` (`id_breach`),
  CONSTRAINT `FK60b1608fq6mln63eet00ybro6` FOREIGN KEY (`fk_breach`) REFERENCES `breach` (`id_breach`),
  CONSTRAINT `FKbawdcdhpe6wv5970eyw6pi745` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKd82bqpoxg1fe7e53v45252wk7` FOREIGN KEY (`fk_investment`) REFERENCES `investment` (`id_investment`),
  CONSTRAINT `FKpylsnb2nnya4bxtq3xd3kgmi2` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `breach_comments`
--

DROP TABLE IF EXISTS `breach_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `breach_comments` (
  `id_breach_comments` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `attachment_ref` bigint(20) DEFAULT NULL,
  `attachment_name` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_breach` bigint(20) DEFAULT NULL,
  `fk_file` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_breach_comments`),
  KEY `FKeyr68i5tbvwuwya8dh6k8fxv9` (`fk_user_created`),
  KEY `FK7vrvw9dwotrsynq5hc7oiusil` (`fk_user_last_modified`),
  KEY `FKhpm9koepjtpce34atdxuhhcwy` (`fk_breach`),
  KEY `FKte9s68yc70ku0khp543wi0qbh` (`fk_file`),
  CONSTRAINT `FK7vrvw9dwotrsynq5hc7oiusil` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKeyr68i5tbvwuwya8dh6k8fxv9` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKhpm9koepjtpce34atdxuhhcwy` FOREIGN KEY (`fk_breach`) REFERENCES `breach` (`id_breach`),
  CONSTRAINT `FKte9s68yc70ku0khp543wi0qbh` FOREIGN KEY (`fk_file`) REFERENCES `file` (`id_file`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `breach_type`
--

DROP TABLE IF EXISTS `breach_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `breach_type` (
  `id_breach_type` bigint(20) NOT NULL AUTO_INCREMENT,
  `breach_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_breach_type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `broker`
--

DROP TABLE IF EXISTS `broker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `broker` (
  `id_broker` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `is_external` int(11) NOT NULL,
  PRIMARY KEY (`id_broker`),
  KEY `FK_n17gcpqjgdx5xht9nmxgah8sn` (`fk_user_created`),
  KEY `FK_l3tl0au4e1bx5l9xm2fr5kuv1` (`fk_user_last_modified`),
  CONSTRAINT `FK5a5omkp63bvfr8qunffke627v` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_l3tl0au4e1bx5l9xm2fr5kuv1` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_n17gcpqjgdx5xht9nmxgah8sn` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKph1i745tsq4o2v6j1k2tiwyxo` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brokerage`
--

DROP TABLE IF EXISTS `brokerage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brokerage` (
  `id_brokerage` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `account_holder_name` varchar(255) DEFAULT NULL,
  `trd_account_number` varchar(255) DEFAULT NULL,
  `nature` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  `brokerage_type` varchar(255) DEFAULT NULL,
  `broker_name` varchar(255) DEFAULT NULL,
  `fk_broker` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_brokerage`),
  KEY `FK2du2flfy2iv8642imnu2v2u49` (`fk_user_created`),
  KEY `FKfxf08rudsj2b6p9vaf1c73e5q` (`fk_user_last_modified`),
  KEY `FK34r56atg536dx4ik4hil56lyq` (`fk_user`),
  KEY `FKs99ddp1afh24cric7e3rbytpx` (`fk_broker`),
  CONSTRAINT `FK2du2flfy2iv8642imnu2v2u49` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK34r56atg536dx4ik4hil56lyq` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKfxf08rudsj2b6p9vaf1c73e5q` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKs99ddp1afh24cric7e3rbytpx` FOREIGN KEY (`fk_broker`) REFERENCES `broker` (`id_broker`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_question`
--

DROP TABLE IF EXISTS `cc_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cc_question` (
  `id_cc_questions` bigint(20) NOT NULL AUTO_INCREMENT,
  `question_text` longtext DEFAULT NULL,
  `fk_cc_question_category` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_cc_questions`),
  KEY `FK5rgxi22l9s3taavy2i59ra6aq` (`fk_cc_question_category`),
  CONSTRAINT `FK5rgxi22l9s3taavy2i59ra6aq` FOREIGN KEY (`fk_cc_question_category`) REFERENCES `cc_question_category` (`id_question_category`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_question_answer`
--

DROP TABLE IF EXISTS `cc_question_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cc_question_answer` (
  `id_cc_question_answers` bigint(20) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) DEFAULT NULL,
  `details` longtext DEFAULT NULL,
  `fk_conflict_clearance` bigint(20) DEFAULT NULL,
  `fk_cc_question` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_cc_question_answers`),
  KEY `FKhtpw697mbpn81klfi1lscyv77` (`fk_conflict_clearance`),
  KEY `FKf1b79w588jpiql7hmt2dpr6fk` (`fk_cc_question`),
  CONSTRAINT `FKf1b79w588jpiql7hmt2dpr6fk` FOREIGN KEY (`fk_cc_question`) REFERENCES `cc_question` (`id_cc_questions`),
  CONSTRAINT `FKhtpw697mbpn81klfi1lscyv77` FOREIGN KEY (`fk_conflict_clearance`) REFERENCES `conflict_clearance` (`id_conflict_clearance`)
) ENGINE=InnoDB AUTO_INCREMENT=1501 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cc_question_category`
--

DROP TABLE IF EXISTS `cc_question_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cc_question_category` (
  `id_question_category` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_question_category`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conflict_clearance`
--

DROP TABLE IF EXISTS `conflict_clearance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conflict_clearance` (
  `id_conflict_clearance` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `client_business` longtext DEFAULT NULL,
  `client_name` longtext DEFAULT NULL,
  `conflict_reason` longtext DEFAULT NULL,
  `dear_source` longtext DEFAULT NULL,
  `is_conflict` int(11) DEFAULT NULL,
  `opposing_party` longtext DEFAULT NULL,
  `opposing_party_business` longtext DEFAULT NULL,
  `opposing_party_desc` longtext DEFAULT NULL,
  `pipeline_deal` longtext DEFAULT NULL,
  `project_name` varchar(255) NOT NULL,
  `proposed_transation` longtext DEFAULT NULL,
  `singnificant_issue` longtext DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_client_exchange` bigint(20) DEFAULT NULL,
  `fk_grey_list` bigint(20) DEFAULT NULL,
  `fk_opp_party_exchange` bigint(20) DEFAULT NULL,
  `opposing_party_involved` longtext DEFAULT NULL,
  `is_client_listed` bit(1) NOT NULL,
  `is_opposing_party_listed` bit(1) DEFAULT NULL,
  `other_party` longtext DEFAULT NULL,
  `other_party_business` longtext DEFAULT NULL,
  `other_party_desc` longtext DEFAULT NULL,
  `other_party_involved` longtext DEFAULT NULL,
  `fk_other_party_exchange` bigint(20) DEFAULT NULL,
  `is_other_party_listed` bit(1) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_conflict_clearance`),
  KEY `FKt2dvfbrj4rjdt3q3kpp86611r` (`fk_user_created`),
  KEY `FK5ft40vaky4eum793otnkwqjf3` (`fk_user_last_modified`),
  KEY `FK5awoul97qfsurub0nrg382xlr` (`fk_client_exchange`),
  KEY `FKddjd937lj4sh112ag32479j1o` (`fk_grey_list`),
  KEY `FKt3laoh5ioa9ygkr8sws2mkn7d` (`fk_opp_party_exchange`),
  KEY `FKq7beeqg95gtgdl492rln20qua` (`fk_other_party_exchange`),
  CONSTRAINT `FK5awoul97qfsurub0nrg382xlr` FOREIGN KEY (`fk_client_exchange`) REFERENCES `stock_exchange` (`id_stock_exchange`),
  CONSTRAINT `FK5ft40vaky4eum793otnkwqjf3` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKddjd937lj4sh112ag32479j1o` FOREIGN KEY (`fk_grey_list`) REFERENCES `grey_list` (`id_grey_list`),
  CONSTRAINT `FKq7beeqg95gtgdl492rln20qua` FOREIGN KEY (`fk_other_party_exchange`) REFERENCES `stock_exchange` (`id_stock_exchange`),
  CONSTRAINT `FKt2dvfbrj4rjdt3q3kpp86611r` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKt3laoh5ioa9ygkr8sws2mkn7d` FOREIGN KEY (`fk_opp_party_exchange`) REFERENCES `stock_exchange` (`id_stock_exchange`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conflict_clearance_comments`
--

DROP TABLE IF EXISTS `conflict_clearance_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conflict_clearance_comments` (
  `id_conflict_clearance_comments` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `attachment_ref` bigint(20) DEFAULT NULL,
  `attachment_name` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_conflict_clearance` bigint(20) DEFAULT NULL,
  `fk_file` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_conflict_clearance_comments`),
  KEY `FKfoe41vjnjxi269cvwg7tnygit` (`fk_user_created`),
  KEY `FKk6c3qaoly3k6ayot387swvu1e` (`fk_user_last_modified`),
  KEY `FKdkhc0j4fpiw4cmee4ouqpw8kp` (`fk_conflict_clearance`),
  KEY `FKcbnmt7yqpp2ywmghxjihqchkj` (`fk_file`),
  CONSTRAINT `FKcbnmt7yqpp2ywmghxjihqchkj` FOREIGN KEY (`fk_file`) REFERENCES `file` (`id_file`),
  CONSTRAINT `FKdkhc0j4fpiw4cmee4ouqpw8kp` FOREIGN KEY (`fk_conflict_clearance`) REFERENCES `conflict_clearance` (`id_conflict_clearance`),
  CONSTRAINT `FKfoe41vjnjxi269cvwg7tnygit` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKk6c3qaoly3k6ayot387swvu1e` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conflict_clearance_parties`
--

DROP TABLE IF EXISTS `conflict_clearance_parties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conflict_clearance_parties` (
  `id_conflict_clearance_parties` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `is_listed` int(1) DEFAULT NULL,
  `party_name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_conflict_clearance` bigint(20) DEFAULT NULL,
  `fk_exchange` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_conflict_clearance_parties`),
  KEY `FK5c6v2t6r58n8b2b0ef6q7reby` (`fk_user_created`),
  KEY `FK81j2bodwvy62otq0qkjgfsiao` (`fk_user_last_modified`),
  KEY `FKby29514ybaqcodkhgtq81bhma` (`fk_conflict_clearance`),
  KEY `FKrmoxmb3wfcwfuelcyldtjbcsk` (`fk_exchange`),
  CONSTRAINT `FK5c6v2t6r58n8b2b0ef6q7reby` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK81j2bodwvy62otq0qkjgfsiao` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKby29514ybaqcodkhgtq81bhma` FOREIGN KEY (`fk_conflict_clearance`) REFERENCES `conflict_clearance` (`id_conflict_clearance`),
  CONSTRAINT `FKrmoxmb3wfcwfuelcyldtjbcsk` FOREIGN KEY (`fk_exchange`) REFERENCES `stock_exchange` (`id_stock_exchange`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `counter`
--

DROP TABLE IF EXISTS `counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `counter` (
  `id_counter` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_stock_exchange` bigint(20) DEFAULT NULL,
  `isViewable` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_counter`),
  KEY `FK_oa6s5axr0rt79740b1utqalhr` (`fk_user_created`),
  KEY `FK_i25wodts8bor2jennv2q6ka9v` (`fk_user_last_modified`),
  KEY `FK_aj0l64c2127wxpnsiw9mpd18l` (`fk_stock_exchange`),
  CONSTRAINT `FK50ukaqx6hdvtuko5uv2wgerol` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKbymev5vvxwyoymosbjdkrx122` FOREIGN KEY (`fk_stock_exchange`) REFERENCES `stock_exchange` (`id_stock_exchange`),
  CONSTRAINT `FKl3kew4txi52tr2fskdpakfuuq` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=88710 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id_country` bigint(20) NOT NULL AUTO_INCREMENT,
  `iso2_code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=318 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deal_team_member`
--

DROP TABLE IF EXISTS `deal_team_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deal_team_member` (
  `id_deal_team_member` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_grey_list` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  `member_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_deal_team_member`),
  KEY `FK_i53ydf31xb0mcnni2vdrsn4xh` (`fk_user_created`),
  KEY `FK_8fkdebl0vlowml2f7u3mxkkon` (`fk_user_last_modified`),
  KEY `FK_qu12s70ubkcpjep6p4ds1hnrb` (`fk_grey_list`),
  KEY `FK_jys33j889wfqv0sdx768pes6g` (`fk_user`),
  CONSTRAINT `FK4tq1ds45db6b92ovejn6iv0xf` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK7l02f58r4k2s9902p8pog2fjp` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_8fkdebl0vlowml2f7u3mxkkon` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_i53ydf31xb0mcnni2vdrsn4xh` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_jys33j889wfqv0sdx768pes6g` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_qu12s70ubkcpjep6p4ds1hnrb` FOREIGN KEY (`fk_grey_list`) REFERENCES `grey_list` (`id_grey_list`),
  CONSTRAINT `FKdgdgruqlbm2urqmvih806879d` FOREIGN KEY (`fk_grey_list`) REFERENCES `grey_list` (`id_grey_list`),
  CONSTRAINT `FKecti8rgbka6ibo8btvkky7d01` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=1113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deal_team_member_copy`
--

DROP TABLE IF EXISTS `deal_team_member_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deal_team_member_copy` (
  `id_deal_team_member_copy` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_grey_list_copy` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  `member_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_deal_team_member_copy`),
  KEY `FKrri5jgtqoufextvfjkoedudfs` (`fk_user_created`),
  KEY `FK7oxkxm5k427bwxwe6w9qritpx` (`fk_user_last_modified`),
  KEY `FKntwh8i8oxmh1l2egsin1cplq2` (`fk_grey_list_copy`),
  KEY `FKirltft9nh5j7sox1kgvroijk9` (`fk_user`),
  CONSTRAINT `FK7oxkxm5k427bwxwe6w9qritpx` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKirltft9nh5j7sox1kgvroijk9` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKntwh8i8oxmh1l2egsin1cplq2` FOREIGN KEY (`fk_grey_list_copy`) REFERENCES `grey_list_copy` (`id_grey_list_copy`),
  CONSTRAINT `FKrri5jgtqoufextvfjkoedudfs` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=10964 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id_department` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_country` bigint(20) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `org_structure_level_4` varchar(255) DEFAULT NULL,
  `org_structure_level_4_desc` varchar(255) DEFAULT NULL,
  `org_structure_level_5` varchar(255) DEFAULT NULL,
  `org_structure_level_5_desc` varchar(255) DEFAULT NULL,
  `org_structure_level_10` varchar(255) DEFAULT NULL,
  `org_structure_level_10_desc` varchar(255) DEFAULT NULL,
  `org_structure_level_6` varchar(255) DEFAULT NULL,
  `org_structure_level_6_desc` varchar(255) DEFAULT NULL,
  `org_structure_level_7` varchar(255) DEFAULT NULL,
  `org_structure_level_7_desc` varchar(255) DEFAULT NULL,
  `org_structure_level_8` varchar(255) DEFAULT NULL,
  `org_structure_level_8_desc` varchar(255) DEFAULT NULL,
  `org_structure_level_9` varchar(255) DEFAULT NULL,
  `org_structure_level_9_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_department`),
  KEY `FK_3r28u5oqkdv000xarfpvuiarf` (`fk_user_created`),
  KEY `FK_676e9c57juf3jjbb3n0f9evjo` (`fk_user_last_modified`),
  KEY `FK_m12kneibohg65r49d09wkyvrj` (`fk_country`),
  CONSTRAINT `FK_3r28u5oqkdv000xarfpvuiarf` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_676e9c57juf3jjbb3n0f9evjo` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_m12kneibohg65r49d09wkyvrj` FOREIGN KEY (`fk_country`) REFERENCES `country` (`id_country`),
  CONSTRAINT `FKcgax965f97dhq72438ji4sajf` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKhnbxugbfve69y6vp3t5bxtbe1` FOREIGN KEY (`fk_country`) REFERENCES `country` (`id_country`),
  CONSTRAINT `FKkh01lbbbg8efqlnqu5flj59da` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2047 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `family_member`
--

DROP TABLE IF EXISTS `family_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family_member` (
  `id_family_member` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `nric` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_relationship_type` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_family_member`),
  KEY `FK567esh9d5qiabxpm9enveud1p` (`fk_user_created`),
  KEY `FKd2xnppvcobalyoskl9dlmam25` (`fk_user_last_modified`),
  KEY `FKjm6ldh7cd7nila4kf43buqav9` (`fk_relationship_type`),
  KEY `FK52kf6ojixv70e2evivfhbpp1b` (`fk_user`),
  CONSTRAINT `FK52kf6ojixv70e2evivfhbpp1b` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK567esh9d5qiabxpm9enveud1p` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKd2xnppvcobalyoskl9dlmam25` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKjm6ldh7cd7nila4kf43buqav9` FOREIGN KEY (`fk_relationship_type`) REFERENCES `relationship_type` (`id_relationship_type`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `id_file` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `original_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grey_list`
--

DROP TABLE IF EXISTS `grey_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grey_list` (
  `id_grey_list` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `project_name` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `parent_grey_list_id` bigint(20) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `is_expire_now` int(11) NOT NULL,
  `is_new` int(11) NOT NULL,
  `revision_no` bigint(20) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  PRIMARY KEY (`id_grey_list`),
  KEY `FK_trv6sog14408srplsnqlc6t7j` (`fk_user_created`),
  KEY `FK_ma9e846a9m6v1ncrtq0qf9enq` (`fk_user_last_modified`),
  KEY `FKmpx3rfl6picok2cu3crh9iur1` (`parent_grey_list_id`),
  CONSTRAINT `FK4ousb39lcirkbs7y3qa5gpf0i` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_3snmw3py0f0rtaulh0n37qs3` FOREIGN KEY (`parent_grey_list_id`) REFERENCES `grey_list` (`id_grey_list`),
  CONSTRAINT `FK_ma9e846a9m6v1ncrtq0qf9enq` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_trv6sog14408srplsnqlc6t7j` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKgm5p9u8c5ol9flodgem3ogpw9` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKmpx3rfl6picok2cu3crh9iur1` FOREIGN KEY (`parent_grey_list_id`) REFERENCES `grey_list` (`id_grey_list`)
) ENGINE=InnoDB AUTO_INCREMENT=616 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grey_list_copy`
--

DROP TABLE IF EXISTS `grey_list_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grey_list_copy` (
  `id_grey_list_copy` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `is_expire_now` int(11) NOT NULL,
  `is_new` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `revision_no` bigint(20) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_parent_greyList` bigint(20) DEFAULT NULL,
  `fk_audit_log` bigint(20) DEFAULT NULL,
  `audit_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_grey_list_copy`),
  KEY `FK9yn29imaitfjweip9j504th0o` (`fk_user_created`),
  KEY `FKb2xfsdmni2egc7rjsb1oekxe7` (`fk_user_last_modified`),
  KEY `FKnq1qn9jwr0ykyqtpyfle8vxqx` (`fk_parent_greyList`),
  KEY `FKo0w3gg48hfwdeqsbe7joctvab` (`fk_audit_log`),
  KEY `last_modified_date` (`last_modified_date`),
  CONSTRAINT `FK9yn29imaitfjweip9j504th0o` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKb2xfsdmni2egc7rjsb1oekxe7` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKnq1qn9jwr0ykyqtpyfle8vxqx` FOREIGN KEY (`fk_parent_greyList`) REFERENCES `grey_list` (`id_grey_list`),
  CONSTRAINT `FKo0w3gg48hfwdeqsbe7joctvab` FOREIGN KEY (`fk_audit_log`) REFERENCES `audit_log` (`id_audit_log`)
) ENGINE=InnoDB AUTO_INCREMENT=4089 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grey_list_department`
--

DROP TABLE IF EXISTS `grey_list_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grey_list_department` (
  `id_grey_list_department` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_department` bigint(20) DEFAULT NULL,
  `fk_grey_list` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_grey_list_department`),
  KEY `FK_7hb4e47w6r04j2gqoleww4997` (`fk_user_created`),
  KEY `FK_ji7m6x2esdokok89b8xcf3a6q` (`fk_user_last_modified`),
  KEY `FK_79kxe61yugyu6hbuhs2xa4wlp` (`fk_department`),
  KEY `FK_qy7lcqslfrhakq3gb3co074bs` (`fk_grey_list`),
  CONSTRAINT `FK4gxyvy7uvsdn5rednoj1285qd` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK9mtboafl7ebjbvq0uvqmfp3ip` FOREIGN KEY (`fk_department`) REFERENCES `department` (`id_department`),
  CONSTRAINT `FK_79kxe61yugyu6hbuhs2xa4wlp` FOREIGN KEY (`fk_department`) REFERENCES `department` (`id_department`),
  CONSTRAINT `FK_7hb4e47w6r04j2gqoleww4997` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_ji7m6x2esdokok89b8xcf3a6q` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_qy7lcqslfrhakq3gb3co074bs` FOREIGN KEY (`fk_grey_list`) REFERENCES `grey_list` (`id_grey_list`),
  CONSTRAINT `FKsconufuymvck2be27vv28aja8` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKsh7ukrgc42qiblge39k268lw9` FOREIGN KEY (`fk_grey_list`) REFERENCES `grey_list` (`id_grey_list`)
) ENGINE=InnoDB AUTO_INCREMENT=917 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grey_list_department_copy`
--

DROP TABLE IF EXISTS `grey_list_department_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grey_list_department_copy` (
  `id_grey_list_department_copy` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_department` bigint(20) DEFAULT NULL,
  `fk_grey_list_copy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_grey_list_department_copy`),
  KEY `FKqfiogcwfudm7nwpot8gwcappj` (`fk_user_created`),
  KEY `FKsby8mh4on4r536m2hobxbuq9t` (`fk_user_last_modified`),
  KEY `FK32vavjmjrgekdlkx0x0or851s` (`fk_department`),
  KEY `FKs47qj34ydpu2pga8160ks8k90` (`fk_grey_list_copy`),
  CONSTRAINT `FK32vavjmjrgekdlkx0x0or851s` FOREIGN KEY (`fk_department`) REFERENCES `department` (`id_department`),
  CONSTRAINT `FKqfiogcwfudm7nwpot8gwcappj` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKs47qj34ydpu2pga8160ks8k90` FOREIGN KEY (`fk_grey_list_copy`) REFERENCES `grey_list_copy` (`id_grey_list_copy`),
  CONSTRAINT `FKsby8mh4on4r536m2hobxbuq9t` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=8043 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grey_list_notification`
--

DROP TABLE IF EXISTS `grey_list_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grey_list_notification` (
  `id_grey_list_notification` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_grey_list` bigint(20) NOT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  `fk_upload_statement` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_grey_list_notification`),
  KEY `FKo5oce4863kd74qcn0982excjm` (`fk_user_created`),
  KEY `FKat9s0bcphgqaphu6656lq6xjm` (`fk_user_last_modified`),
  KEY `FKe4bi6ua2hvhiolb2xpx73kbo4` (`fk_grey_list`),
  KEY `FK7n2bi2owbxjes1y5msvel1a9x` (`fk_user`),
  KEY `FK9le1ewwmoxby0wwy6d2jv8uq6` (`fk_upload_statement`),
  CONSTRAINT `FK7n2bi2owbxjes1y5msvel1a9x` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK9le1ewwmoxby0wwy6d2jv8uq6` FOREIGN KEY (`fk_upload_statement`) REFERENCES `upload_statement` (`id_upload_statement`),
  CONSTRAINT `FKat9s0bcphgqaphu6656lq6xjm` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKe4bi6ua2hvhiolb2xpx73kbo4` FOREIGN KEY (`fk_grey_list`) REFERENCES `grey_list` (`id_grey_list`),
  CONSTRAINT `FKo5oce4863kd74qcn0982excjm` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=859 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grey_list_share_counter`
--

DROP TABLE IF EXISTS `grey_list_share_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grey_list_share_counter` (
  `id_grey_list_counter` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_counter` bigint(20) NOT NULL,
  `fk_grey_list` bigint(20) NOT NULL,
  PRIMARY KEY (`id_grey_list_counter`),
  KEY `FKfo9upww5p2rieijglde3u1mca` (`fk_user_created`),
  KEY `FKpnqh46xxxkdygmj4ompjvf7el` (`fk_user_last_modified`),
  KEY `FKbjcqpxx708kj6fracptd1256f` (`fk_counter`),
  KEY `FKdaf1cyfyv20qygyr403ti21cf` (`fk_grey_list`),
  CONSTRAINT `FKbjcqpxx708kj6fracptd1256f` FOREIGN KEY (`fk_counter`) REFERENCES `counter` (`id_counter`),
  CONSTRAINT `FKdaf1cyfyv20qygyr403ti21cf` FOREIGN KEY (`fk_grey_list`) REFERENCES `grey_list` (`id_grey_list`),
  CONSTRAINT `FKfo9upww5p2rieijglde3u1mca` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKpnqh46xxxkdygmj4ompjvf7el` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=451 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grey_list_share_counter_copy`
--

DROP TABLE IF EXISTS `grey_list_share_counter_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grey_list_share_counter_copy` (
  `id_grey_list_counter_copy` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_counter` bigint(20) NOT NULL,
  `fk_grey_list_copy` bigint(20) NOT NULL,
  PRIMARY KEY (`id_grey_list_counter_copy`),
  KEY `FKoa7ayx60touuuhsnmm8bgp8p6` (`fk_user_created`),
  KEY `FKdw49f1526bemeqqqrs0fwoly3` (`fk_user_last_modified`),
  KEY `FKb50ik633xsgag6jmqrorxi0l3` (`fk_counter`),
  KEY `FKci5f64tor2xu6e7dimftflajg` (`fk_grey_list_copy`),
  CONSTRAINT `FKb50ik633xsgag6jmqrorxi0l3` FOREIGN KEY (`fk_counter`) REFERENCES `counter` (`id_counter`),
  CONSTRAINT `FKci5f64tor2xu6e7dimftflajg` FOREIGN KEY (`fk_grey_list_copy`) REFERENCES `grey_list_copy` (`id_grey_list_copy`),
  CONSTRAINT `FKdw49f1526bemeqqqrs0fwoly3` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKoa7ayx60touuuhsnmm8bgp8p6` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5047 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_department`
--

DROP TABLE IF EXISTS `group_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_department` (
  `id_group_department` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_department` bigint(20) DEFAULT NULL,
  `fk_group_info` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_group_department`),
  KEY `FK_fa66un256vyg6a55daiyfv7c6` (`fk_user_created`),
  KEY `FK_pnbb8pelj76rst9xd8sdgvjyj` (`fk_user_last_modified`),
  KEY `FK_cskmu98jc36333ytb9at2by0j` (`fk_department`),
  KEY `FK_heathfnhuh0sxky9h5gr85xh8` (`fk_group_info`),
  CONSTRAINT `FK1qcrd34a6kw316nuhdx5tw4et` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK5wb26vu01nwli3l95ohtnbpww` FOREIGN KEY (`fk_group_info`) REFERENCES `group_info` (`id_group_info`),
  CONSTRAINT `FK_cskmu98jc36333ytb9at2by0j` FOREIGN KEY (`fk_department`) REFERENCES `department` (`id_department`),
  CONSTRAINT `FK_fa66un256vyg6a55daiyfv7c6` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_heathfnhuh0sxky9h5gr85xh8` FOREIGN KEY (`fk_group_info`) REFERENCES `group_info` (`id_group_info`),
  CONSTRAINT `FK_pnbb8pelj76rst9xd8sdgvjyj` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKe1vmj3i16a6ppgesp07mf8kbl` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKhsecei0w71839kt0lopqkg4rs` FOREIGN KEY (`fk_department`) REFERENCES `department` (`id_department`)
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_info`
--

DROP TABLE IF EXISTS `group_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_info` (
  `id_group_info` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `group_code` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `revision_no` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_group_info`),
  UNIQUE KEY `UK_kthciaymtcpspfdcf9jdkqw1w` (`group_code`),
  UNIQUE KEY `UK_6n3i8c7pvh53b26qxlm3fsh1f` (`group_name`),
  KEY `FK_bq8jhtt7tbqsj5epr6qd09vs6` (`fk_user_created`),
  KEY `FK_sq9qe5mrtk0v9pyke2i8uy1wc` (`fk_user_last_modified`),
  CONSTRAINT `FK_bq8jhtt7tbqsj5epr6qd09vs6` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_sq9qe5mrtk0v9pyke2i8uy1wc` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKel2kp1fgh2ke279ktj8csw911` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKmurap2isf79c834eyedmg3fvp` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_info_copy`
--

DROP TABLE IF EXISTS `group_info_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_info_copy` (
  `id_group_info_copy` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `audit_type` varchar(255) DEFAULT NULL,
  `group_code` varchar(255) DEFAULT NULL,
  `group_department` text DEFAULT NULL,
  `group_member_name` text DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `revision_no` bigint(20) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_group_info` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_group_info_copy`),
  KEY `FKlfuxvdl5ikkgnwv1uqwufjo18` (`fk_user_created`),
  KEY `FKde7bc6c86fsyiwscejaukiq7v` (`fk_user_last_modified`),
  KEY `FKk8wq2u7chbs5qsfjys1xgnls2` (`fk_group_info`),
  CONSTRAINT `FKde7bc6c86fsyiwscejaukiq7v` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKk8wq2u7chbs5qsfjys1xgnls2` FOREIGN KEY (`fk_group_info`) REFERENCES `group_info` (`id_group_info`),
  CONSTRAINT `FKlfuxvdl5ikkgnwv1uqwufjo18` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=471 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_user`
--

DROP TABLE IF EXISTS `group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_user` (
  `id_group_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_group_info` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_group_user`),
  KEY `FK_1jr41to4e84gnoij7am0hg0ry` (`fk_user_created`),
  KEY `FK_ko5unxiheak7c4k073xek449x` (`fk_user_last_modified`),
  KEY `FK_f3jdhw4d9v9c8ept5goolfat` (`fk_group_info`),
  KEY `FK_n3086mndi5ey0bagosnk2nopf` (`fk_user`),
  CONSTRAINT `FK39gdl76n70q7e7b3wumanokf7` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK472jk3ylcroub5ohda26kxslj` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_1jr41to4e84gnoij7am0hg0ry` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_f3jdhw4d9v9c8ept5goolfat` FOREIGN KEY (`fk_group_info`) REFERENCES `group_info` (`id_group_info`),
  CONSTRAINT `FK_ko5unxiheak7c4k073xek449x` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_n3086mndi5ey0bagosnk2nopf` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKbde6lk5vvqbu7s5fd014k3clb` FOREIGN KEY (`fk_group_info`) REFERENCES `group_info` (`id_group_info`),
  CONSTRAINT `FKflwsr781ncq880vhnqkjiv9ig` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `investment`
--

DROP TABLE IF EXISTS `investment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investment` (
  `id_investment` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `activity` int(11) NOT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `sosScheme` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_application` bigint(20) DEFAULT NULL,
  `fk_broker` bigint(20) DEFAULT NULL,
  `fk_counter` bigint(20) DEFAULT NULL,
  `fk_investment_type` bigint(20) DEFAULT NULL,
  `fk_relationship_type` bigint(20) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `fk_family_member` bigint(20) DEFAULT NULL,
  `fk_user_applied` bigint(20) DEFAULT NULL,
  `fk_upload_statement` bigint(20) DEFAULT NULL,
  `agree_to_applications` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_investment`),
  KEY `FK2h4l1p7joh4ujg79k04s7v64l` (`fk_user_created`),
  KEY `FKenejw2yy7c45i29pxocjprhfr` (`fk_user_last_modified`),
  KEY `FKa9fvv8atk33haxw6yx4qt8xxn` (`fk_application`),
  KEY `FKeggo526mbfmf6ue5kc61e7t10` (`fk_broker`),
  KEY `FK3jkwklecbf7ynv3ilc793asxa` (`fk_counter`),
  KEY `FK1igipil5a11fpa14bfjppg8tp` (`fk_investment_type`),
  KEY `FK95l4lp2ckuexrh48tnnrvbjnf` (`fk_relationship_type`),
  KEY `FKqwpdu6210t8sx8g0g4xi1tbx3` (`fk_family_member`),
  KEY `FKpfsb7s7bkn7dihb6b6praw0eb` (`fk_user_applied`),
  KEY `FKp6x0cxrfl2op44yov9slu62ax` (`fk_upload_statement`),
  CONSTRAINT `FK1igipil5a11fpa14bfjppg8tp` FOREIGN KEY (`fk_investment_type`) REFERENCES `investment_type` (`id_investment_type`),
  CONSTRAINT `FK2h4l1p7joh4ujg79k04s7v64l` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK3jkwklecbf7ynv3ilc793asxa` FOREIGN KEY (`fk_counter`) REFERENCES `counter` (`id_counter`),
  CONSTRAINT `FK95l4lp2ckuexrh48tnnrvbjnf` FOREIGN KEY (`fk_relationship_type`) REFERENCES `relationship_type` (`id_relationship_type`),
  CONSTRAINT `FKa9fvv8atk33haxw6yx4qt8xxn` FOREIGN KEY (`fk_application`) REFERENCES `application` (`id_application`),
  CONSTRAINT `FKeggo526mbfmf6ue5kc61e7t10` FOREIGN KEY (`fk_broker`) REFERENCES `broker` (`id_broker`),
  CONSTRAINT `FKenejw2yy7c45i29pxocjprhfr` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKp6x0cxrfl2op44yov9slu62ax` FOREIGN KEY (`fk_upload_statement`) REFERENCES `upload_statement` (`id_upload_statement`),
  CONSTRAINT `FKpfsb7s7bkn7dihb6b6praw0eb` FOREIGN KEY (`fk_user_applied`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKqwpdu6210t8sx8g0g4xi1tbx3` FOREIGN KEY (`fk_family_member`) REFERENCES `family_member` (`id_family_member`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `investment_type`
--

DROP TABLE IF EXISTS `investment_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investment_type` (
  `id_investment_type` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_investment_type`),
  KEY `FKnekagavuyvrbg68kga4jgbc2w` (`fk_user_created`),
  KEY `FKhodo0tu2a0fppyvonw2snrr5b` (`fk_user_last_modified`),
  CONSTRAINT `FKhodo0tu2a0fppyvonw2snrr5b` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKnekagavuyvrbg68kga4jgbc2w` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `legal_vehicle`
--

DROP TABLE IF EXISTS `legal_vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legal_vehicle` (
  `id_legal_vehicle` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `legal_ic` varchar(255) DEFAULT NULL,
  `legal_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_relationship_type` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_legal_vehicle`),
  KEY `FK3ombkr8u1iy2jkd0avynp7rrk` (`fk_user_created`),
  KEY `FKjxigiuwemihvjsk24jh3w4lwc` (`fk_user_last_modified`),
  KEY `FKghye6kfxed2n9jikp1vwajo2t` (`fk_relationship_type`),
  KEY `FK19e8xklay78viccl71hnhg02p` (`fk_user`),
  CONSTRAINT `FK19e8xklay78viccl71hnhg02p` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK3ombkr8u1iy2jkd0avynp7rrk` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKghye6kfxed2n9jikp1vwajo2t` FOREIGN KEY (`fk_relationship_type`) REFERENCES `relationship_type` (`id_relationship_type`),
  CONSTRAINT `FKjxigiuwemihvjsk24jh3w4lwc` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `other_personnal`
--

DROP TABLE IF EXISTS `other_personnal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_personnal` (
  `id_other_personnal` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_grey_list` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_other_personnal`),
  KEY `FKg6kiq3g1vmko2dv0ajh91squy` (`fk_user_created`),
  KEY `FK5u0uwddx12a95b0swhp4m3ui9` (`fk_user_last_modified`),
  KEY `FKj9k4rlewhy1n84tj17clgqhpi` (`fk_grey_list`),
  KEY `FK8rcttqitgrr1alqeaw7xwxdr2` (`fk_user`),
  CONSTRAINT `FK5u0uwddx12a95b0swhp4m3ui9` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK8rcttqitgrr1alqeaw7xwxdr2` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKg6kiq3g1vmko2dv0ajh91squy` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKj9k4rlewhy1n84tj17clgqhpi` FOREIGN KEY (`fk_grey_list`) REFERENCES `grey_list` (`id_grey_list`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `other_personnal_copy`
--

DROP TABLE IF EXISTS `other_personnal_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_personnal_copy` (
  `id_other_personnal_copy` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `other_personnal_ids` text DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_grey_list_copy` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_other_personnal_copy`),
  KEY `FKgevq53n4524hprip5rcwt3ian` (`fk_user_created`),
  KEY `FKdwfv7dte00df6660xi6mtm7op` (`fk_user_last_modified`),
  KEY `FKnie3mcep5lb7cb1oylua7y2a2` (`fk_grey_list_copy`),
  CONSTRAINT `FKdwfv7dte00df6660xi6mtm7op` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKgevq53n4524hprip5rcwt3ian` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKnie3mcep5lb7cb1oylua7y2a2` FOREIGN KEY (`fk_grey_list_copy`) REFERENCES `grey_list_copy` (`id_grey_list_copy`)
) ENGINE=InnoDB AUTO_INCREMENT=3587 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pips_app`
--

DROP TABLE IF EXISTS `pips_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pips_app` (
  `id_pips_app` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  PRIMARY KEY (`id_pips_app`),
  UNIQUE KEY `UK_98fwc4avsfs4hbmjlblecch24` (`app_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pips_group`
--

DROP TABLE IF EXISTS `pips_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pips_group` (
  `id_pips_group` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `fk_pips_app` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_pips_group`),
  KEY `FKf9brshqq2ylxa527miweou418` (`fk_pips_app`),
  CONSTRAINT `FKf9brshqq2ylxa527miweou418` FOREIGN KEY (`fk_pips_app`) REFERENCES `pips_app` (`id_pips_app`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pips_role`
--

DROP TABLE IF EXISTS `pips_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pips_role` (
  `id_pips_role` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_active` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `fk_pips_group` bigint(20) NOT NULL,
  PRIMARY KEY (`id_pips_role`),
  KEY `FKfhqrsima00hcqbpxom7gkt724` (`fk_pips_group`),
  CONSTRAINT `FKfhqrsima00hcqbpxom7gkt724` FOREIGN KEY (`fk_pips_group`) REFERENCES `pips_group` (`id_pips_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `public_holiday`
--

DROP TABLE IF EXISTS `public_holiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `public_holiday` (
  `id_public_holiday` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `holiday_date` date DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_stock_exchange` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_public_holiday`),
  KEY `FKavyktve7glgi0ewif4q4nalwe` (`fk_user_created`),
  KEY `FK25yh6ll178greww4qhte4w5ep` (`fk_user_last_modified`),
  KEY `FKk8orte8y6qja2uysf87ne8yui` (`fk_stock_exchange`),
  CONSTRAINT `FK25yh6ll178greww4qhte4w5ep` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKavyktve7glgi0ewif4q4nalwe` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKk8orte8y6qja2uysf87ne8yui` FOREIGN KEY (`fk_stock_exchange`) REFERENCES `stock_exchange` (`id_stock_exchange`)
) ENGINE=InnoDB AUTO_INCREMENT=1378 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `relationship_type`
--

DROP TABLE IF EXISTS `relationship_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship_type` (
  `id_relationship_type` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_relationship_type`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `securities_holding`
--

DROP TABLE IF EXISTS `securities_holding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `securities_holding` (
  `id_securities_holding` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `securities_costodian` varchar(255) DEFAULT NULL,
  `securities_name` varchar(255) DEFAULT NULL,
  `securities_quantity` bigint(20) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_exchange` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_securities_holding`),
  KEY `FKdhj5vj7ck0s9n9k8a308etqr7` (`fk_user_created`),
  KEY `FK9f5r3ppsl8o6ormo3wn1dd9on` (`fk_user_last_modified`),
  KEY `FKce8v9uun2o8ynj4lmxva57wp6` (`fk_exchange`),
  KEY `FKgt9ogr4kl3o5c1fdsucc4hoba` (`fk_user`),
  CONSTRAINT `FK9f5r3ppsl8o6ormo3wn1dd9on` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKce8v9uun2o8ynj4lmxva57wp6` FOREIGN KEY (`fk_exchange`) REFERENCES `stock_exchange` (`id_stock_exchange`),
  CONSTRAINT `FKdhj5vj7ck0s9n9k8a308etqr7` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKgt9ogr4kl3o5c1fdsucc4hoba` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock_exchange`
--

DROP TABLE IF EXISTS `stock_exchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_exchange` (
  `id_stock_exchange` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_country` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_stock_exchange`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  KEY `FK_ipan9q7i66eetb5f1152mwx1o` (`fk_user_created`),
  KEY `FK_cqv9dyteee0s23hd3r8eq7c60` (`fk_user_last_modified`),
  KEY `FK_k3l0dxdxq6h2k8esyhddqd66i` (`fk_country`),
  CONSTRAINT `FKbmda10xpsgqd3wrrqb813hpt7` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKm6bk5nv59ovwfrk4cfskhfi5s` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKn4yj61dp3hb8a7omi8k3ypm3a` FOREIGN KEY (`fk_country`) REFERENCES `country` (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `upload_notification`
--

DROP TABLE IF EXISTS `upload_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_notification` (
  `id_upload_notification` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_upload_statement` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  `is_statement_uploaded` int(11) NOT NULL,
  PRIMARY KEY (`id_upload_notification`),
  KEY `FKd1849gxgm16pvre75sxa4d8fi` (`fk_user_created`),
  KEY `FK7g57j4f7msqjcfww78639f19g` (`fk_user_last_modified`),
  KEY `FKqfdww7mn4tv20x1bs7ub06fhj` (`fk_upload_statement`),
  KEY `FKg1yxfukcx2bfur5wq7v96eh5c` (`fk_user`),
  CONSTRAINT `FK7g57j4f7msqjcfww78639f19g` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKd1849gxgm16pvre75sxa4d8fi` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKg1yxfukcx2bfur5wq7v96eh5c` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKqfdww7mn4tv20x1bs7ub06fhj` FOREIGN KEY (`fk_upload_statement`) REFERENCES `upload_statement` (`id_upload_statement`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `upload_statement`
--

DROP TABLE IF EXISTS `upload_statement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_statement` (
  `id_upload_statement` bigint(20) NOT NULL AUTO_INCREMENT,
  `agree_to_applications` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `fk_file` bigint(20) DEFAULT NULL,
  `fk_investment` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_upload_statement`),
  KEY `FKkvksjqw6odkaf3qiu2eel0jhd` (`fk_file`),
  KEY `FKtkl9gy4rgxo0po5vaudslqwhx` (`fk_investment`),
  CONSTRAINT `FKkvksjqw6odkaf3qiu2eel0jhd` FOREIGN KEY (`fk_file`) REFERENCES `file` (`id_file`),
  CONSTRAINT `FKtkl9gy4rgxo0po5vaudslqwhx` FOREIGN KEY (`fk_investment`) REFERENCES `investment` (`id_investment`)
) ENGINE=InnoDB AUTO_INCREMENT=204 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `bank_id` varchar(255) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_country` bigint(20) DEFAULT NULL,
  `fk_department` bigint(20) DEFAULT NULL,
  `nric` varchar(255) DEFAULT NULL,
  `is_covered` tinyint(1) DEFAULT 0,
  `has_declared` tinyint(1) DEFAULT 0,
  `last_notified_date` datetime DEFAULT NULL,
  `covered_date` date DEFAULT NULL,
  `cr_roles_deactivated_date` datetime DEFAULT NULL,
  `cr_roles_activated_date` datetime DEFAULT NULL,
  `revision_no` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `UK_2i02agodfj04u49f7q89hysuj` (`bank_id`),
  KEY `FK_8lvv3whgeimbdh9ea0envvxgc` (`fk_user_created`),
  KEY `FK_e90xgh17i6o9d2omrg2bd1mp1` (`fk_user_last_modified`),
  KEY `FK_jhbklrx7dd66hrpjs3n34m7bi` (`fk_country`),
  KEY `FK_c539xsauasequfc5e0vyjifjx` (`fk_department`),
  CONSTRAINT `FK64wegrxbk0tgbdc4tbgppdnaq` FOREIGN KEY (`fk_department`) REFERENCES `department` (`id_department`),
  CONSTRAINT `FK6i37ywv42ge5g5suaaeak0dlj` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_8lvv3whgeimbdh9ea0envvxgc` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_c539xsauasequfc5e0vyjifjx` FOREIGN KEY (`fk_department`) REFERENCES `department` (`id_department`),
  CONSTRAINT `FK_e90xgh17i6o9d2omrg2bd1mp1` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FK_jhbklrx7dd66hrpjs3n34m7bi` FOREIGN KEY (`fk_country`) REFERENCES `country` (`id_country`),
  CONSTRAINT `FKhvhau01x3x93fft93jyfs0390` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKnny4wypm3985ndcl5ckp5dvxb` FOREIGN KEY (`fk_country`) REFERENCES `country` (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=112597 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_app_group`
--

DROP TABLE IF EXISTS `user_app_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_app_group` (
  `id_user_app_group` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_pips_group` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_user_app_group`),
  KEY `FKl5lhegfd2tjgg1m8kg51t4vqh` (`fk_user_created`),
  KEY `FK3k8ow6grjgm81r9asyk77iuh1` (`fk_user_last_modified`),
  KEY `FKj2svs5h5veg8ms0ls3q5h2y1f` (`fk_pips_group`),
  KEY `FKkkfmb13yijkousxf41b6rgqc0` (`fk_user`),
  CONSTRAINT `FK3k8ow6grjgm81r9asyk77iuh1` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKj2svs5h5veg8ms0ls3q5h2y1f` FOREIGN KEY (`fk_pips_group`) REFERENCES `pips_group` (`id_pips_group`),
  CONSTRAINT `FKkkfmb13yijkousxf41b6rgqc0` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKl5lhegfd2tjgg1m8kg51t4vqh` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_app_group_copy`
--

DROP TABLE IF EXISTS `user_app_group_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_app_group_copy` (
  `id_user_app_group_copy` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_group_coutry` text DEFAULT NULL,
  `audit_type` varchar(255) DEFAULT NULL,
  `bank_id` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `preferred_name` varchar(255) DEFAULT NULL,
  `revision_no` bigint(20) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_user_app_group_copy`),
  KEY `FK8rqpfyf4jxwq9xd14tm4oksp2` (`fk_user_created`),
  KEY `FKs8ghv1ncxgyp27kh0gx1p6dui` (`fk_user`),
  CONSTRAINT `FK8rqpfyf4jxwq9xd14tm4oksp2` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKs8ghv1ncxgyp27kh0gx1p6dui` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=399 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_app_group_country`
--

DROP TABLE IF EXISTS `user_app_group_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_app_group_country` (
  `id_user_app_group_country` bigint(20) NOT NULL AUTO_INCREMENT,
  `fk_country` bigint(20) DEFAULT NULL,
  `fk_user_app_group` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_user_app_group_country`),
  KEY `FKlh16bj0hu1jso26h4j633u2g5` (`fk_country`),
  KEY `FKq1uanae9xrjwihsnhwx6v0lcj` (`fk_user_app_group`),
  CONSTRAINT `FKlh16bj0hu1jso26h4j633u2g5` FOREIGN KEY (`fk_country`) REFERENCES `country` (`id_country`),
  CONSTRAINT `FKq1uanae9xrjwihsnhwx6v0lcj` FOREIGN KEY (`fk_user_app_group`) REFERENCES `user_app_group` (`id_user_app_group`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_covered_person`
--

DROP TABLE IF EXISTS `user_covered_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_covered_person` (
  `id_user_covered_person` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `is_covered` int(11) DEFAULT NULL,
  `fk_user_created` bigint(20) DEFAULT NULL,
  `fk_user_last_modified` bigint(20) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  `covered_date` date DEFAULT NULL,
  PRIMARY KEY (`id_user_covered_person`),
  KEY `FKf9lvqewwhntvxq2gj65y7jh6e` (`fk_user_created`),
  KEY `FKl5jam1iarjge9e2qs19694nae` (`fk_user_last_modified`),
  KEY `FKhc6ieo8y02dq71ssxf3309h7d` (`fk_user`),
  CONSTRAINT `FKf9lvqewwhntvxq2gj65y7jh6e` FOREIGN KEY (`fk_user_created`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKhc6ieo8y02dq71ssxf3309h7d` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `FKl5jam1iarjge9e2qs19694nae` FOREIGN KEY (`fk_user_last_modified`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `id_user_info` bigint(20) NOT NULL AUTO_INCREMENT,
  `bank_id` varchar(255) DEFAULT NULL,
  `business_title` varchar(255) DEFAULT NULL,
  `country_manager_id` bigint(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `full_time_part_time` varchar(1) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `job_code` varchar(255) DEFAULT NULL,
  `job_code_descr` varchar(255) DEFAULT NULL,
  `lan_id` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `level10_descr` varchar(255) DEFAULT NULL,
  `level3_descr` varchar(255) DEFAULT NULL,
  `level4_descr` varchar(255) DEFAULT NULL,
  `level5_descr` varchar(255) DEFAULT NULL,
  `level6_descr` varchar(255) DEFAULT NULL,
  `level7_descr` varchar(255) DEFAULT NULL,
  `level8_descr` varchar(255) DEFAULT NULL,
  `level9_descr` varchar(255) DEFAULT NULL,
  `local_name` varchar(255) DEFAULT NULL,
  `nric` varchar(255) DEFAULT NULL,
  `org_structure_level_1` varchar(255) DEFAULT NULL,
  `org_structure_level_10` varchar(255) DEFAULT NULL,
  `org_structure_level_2` varchar(255) DEFAULT NULL,
  `org_structure_level_3` varchar(255) DEFAULT NULL,
  `org_structure_level_4` varchar(255) DEFAULT NULL,
  `org_structure_level_5` varchar(255) DEFAULT NULL,
  `org_structure_level_6` varchar(255) DEFAULT NULL,
  `org_structure_level_7` varchar(255) DEFAULT NULL,
  `org_structure_level_8` varchar(255) DEFAULT NULL,
  `org_structure_level_9` varchar(255) DEFAULT NULL,
  `pref_name_of_regional_bu_manager` varchar(255) DEFAULT NULL,
  `preferred_name` varchar(255) DEFAULT NULL,
  `preffered_name_of_country_manager` varchar(255) DEFAULT NULL,
  `rank` varchar(255) DEFAULT NULL,
  `regional_manager_id` bigint(20) DEFAULT NULL,
  `regular_temporary` varchar(1) DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_user_info`),
  UNIQUE KEY `bank_id_unq` (`bank_id`),
  KEY `FKkmso70cgm0840fxbbis8lnyhx` (`fk_user`),
  CONSTRAINT `FKkmso70cgm0840fxbbis8lnyhx` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=45541 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-14 15:12:16
