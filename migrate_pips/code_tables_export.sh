MYSQL_USER="root"
MYSQL_PASS="password"
SQL_PATH="../sql"
DATABASE="pips"

mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables broker >> "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables cc_question > "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables cc_question_category > "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables pips_app > "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables pips_group > "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables pips_role > "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables user_app_group > "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables user > "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables user_app_group_country > "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables user_covered_person > "${SQL_PATH}/import_code_table.sql"
mysqldump -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} --no-create-info --tables user_info > "${SQL_PATH}/import_code_table.sql"


