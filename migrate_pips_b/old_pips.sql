-- MySQL dump 10.16  Distrib 10.1.17-MariaDB, for osx10.11 (x86_64)
--
-- Host: localhost    Database: old_pips
-- ------------------------------------------------------
-- Server version	10.1.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `A_AssignUserCompany`
--

DROP TABLE IF EXISTS `A_AssignUserCompany`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_AssignUserCompany` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `CompanyID` int(11) DEFAULT NULL,
  `IsDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `A_AssignUserCompany`
--

LOCK TABLES `A_AssignUserCompany` WRITE;
/*!40000 ALTER TABLE `A_AssignUserCompany` DISABLE KEYS */;
/*!40000 ALTER TABLE `A_AssignUserCompany` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `A_AssignUserCountry`
--

DROP TABLE IF EXISTS `A_AssignUserCountry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_AssignUserCountry` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `IsDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `A_AssignUserCountry`
--

LOCK TABLES `A_AssignUserCountry` WRITE;
/*!40000 ALTER TABLE `A_AssignUserCountry` DISABLE KEYS */;
/*!40000 ALTER TABLE `A_AssignUserCountry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `A_AssignUserDept`
--

DROP TABLE IF EXISTS `A_AssignUserDept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_AssignUserDept` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `DeptID` int(11) DEFAULT NULL,
  `IsDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `A_AssignUserDept`
--

LOCK TABLES `A_AssignUserDept` WRITE;
/*!40000 ALTER TABLE `A_AssignUserDept` DISABLE KEYS */;
/*!40000 ALTER TABLE `A_AssignUserDept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `A_BlackoutList`
--

DROP TABLE IF EXISTS `A_BlackoutList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_BlackoutList` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `BlackListID` int(11) DEFAULT NULL,
  `CounterID` int(11) DEFAULT NULL,
  `ListTypeID` int(11) DEFAULT NULL,
  `ResearchStartDate` varchar(255) DEFAULT NULL,
  `ResearchEndDate` varchar(255) DEFAULT NULL,
  `PTStartDate` varchar(255) DEFAULT NULL,
  `PTEndDate` varchar(255) DEFAULT NULL,
  `PIPStartDate` varchar(255) DEFAULT NULL,
  `PIPEndDate` varchar(255) DEFAULT NULL,
  `CSStartDate` varchar(255) DEFAULT NULL,
  `CSEndDate` varchar(255) DEFAULT NULL,
  `Remarks` text,
  `Comments` text,
  `IsDealCompleted` varchar(255) DEFAULT NULL,
  `CreatedDate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `A_BlackoutList`
--

LOCK TABLES `A_BlackoutList` WRITE;
/*!40000 ALTER TABLE `A_BlackoutList` DISABLE KEYS */;
/*!40000 ALTER TABLE `A_BlackoutList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `A_CodeTable_AuthorizedPersonnel`
--

DROP TABLE IF EXISTS `A_CodeTable_AuthorizedPersonnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_CodeTable_AuthorizedPersonnel` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `AuthorizedPersonnelID` int(11) DEFAULT NULL,
  `OneBankID` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `A_CodeTable_AuthorizedPersonnel`
--

LOCK TABLES `A_CodeTable_AuthorizedPersonnel` WRITE;
/*!40000 ALTER TABLE `A_CodeTable_AuthorizedPersonnel` DISABLE KEYS */;
/*!40000 ALTER TABLE `A_CodeTable_AuthorizedPersonnel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `A_CodeTable_Broker`
--

DROP TABLE IF EXISTS `A_CodeTable_Broker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_CodeTable_Broker` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `BrokerID` varchar(255) DEFAULT NULL,
  `BrokerCode` varchar(255) DEFAULT NULL,
  `Broker` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `A_CodeTable_Broker`
--

LOCK TABLES `A_CodeTable_Broker` WRITE;
/*!40000 ALTER TABLE `A_CodeTable_Broker` DISABLE KEYS */;
/*!40000 ALTER TABLE `A_CodeTable_Broker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `A_CodeTable_Company`
--

DROP TABLE IF EXISTS `A_CodeTable_Company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_CodeTable_Company` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyID` int(11) DEFAULT NULL,
  `Company` varchar(255) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `CompanyDescription` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `A_CodeTable_Company`
--

LOCK TABLES `A_CodeTable_Company` WRITE;
/*!40000 ALTER TABLE `A_CodeTable_Company` DISABLE KEYS */;
/*!40000 ALTER TABLE `A_CodeTable_Company` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-12 16:37:26
DROP TABLE IF EXISTS `A_CodeTable_Counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_CodeTable_Counter` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `CounterID` int(11) DEFAULT NULL,
  `CounterCode` varchar(255) DEFAULT NULL,
  `Counter` varchar(255) DEFAULT NULL,
  `StockExchangeID` int(11) DEFAULT NULL,
  `Viewable` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
);

DROP TABLE IF EXISTS `A_CodeTable_Country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_CodeTable_Country` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `CountryID` int(11) DEFAULT NULL,
  `CountryCode` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
);
DROP TABLE IF EXISTS `A_CodeTable_Currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_CodeTable_Currency` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `CurrencyID` int(11) DEFAULT NULL,
  `CurrencyName` varchar(255) DEFAULT NULL,
  `StringCode` varchar(255) DEFAULT NULL,
  `NumCode` INT(11) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
);
DROP TABLE IF EXISTS `A_CodeTable_HolidayList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_CodeTable_HolidayList` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `HolidayListID` int(11) DEFAULT NULL,
  `StockExchangeCode` varchar(255) DEFAULT NULL,
  `HolidayName` varchar(255) DEFAULT NULL,
  `HolidayDate` varchar(100) DEFAULT NULL,
  `Viewable` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
);
DROP TABLE IF EXISTS `A_CodeTable_NotificationMessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_CodeTable_NotificationMessage` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `NotificationID` int(11) DEFAULT NULL,
  `NotificationName` varchar(255) DEFAULT NULL,
  `Subject` text DEFAULT NULL,
  `Body` text DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
);
DROP TABLE IF EXISTS `A_CodeTable_OthersGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `A_CodeTable_OthersGroup` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) DEFAULT NULL,
  `GroupCode` varchar(255) DEFAULT NULL,
  `GroupName` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
);
DROP TABLE IF EXISTS `AssignUserCompany`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssignUserCompany` (
  `AID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) DEFAULT NULL,
  `IsDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AID`)
);
DROP TABLE IF EXISTS `AssignUserCountry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssignUserCountry` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `CountryID` int(11) DEFAULT NULL,
  `IsDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
);
DROP TABLE IF EXISTS `AssignUserDept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssignUserDept` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `DeptID` int(11) DEFAULT NULL,
  `IsDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
);
DROP TABLE IF EXISTS `BlackoutList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlackoutList` (
  `BlackListID` int(11) NOT NULL AUTO_INCREMENT,
  `CounterID` int(11) DEFAULT NULL,
  `ListTypeID` int(11) DEFAULT NULL,
  `ResearchStartDate` varchar(255) DEFAULT NULL,
  `ResearchEndDate` varchar(255) DEFAULT NULL,
  `PTStartDate` varchar(255) DEFAULT NULL,
  `PTEndDate` varchar(255) DEFAULT NULL,
  `PIPStartDate` varchar(255) DEFAULT NULL,
  `PIPEndDate` varchar(255) DEFAULT NULL,
  `CSStartDate` varchar(255) DEFAULT NULL,
  `CSEndDate` varchar(255) DEFAULT NULL,
  `Remarks` text DEFAULT NULL,
  `Comments` text DEFAULT NULL,
  `IsDealCompleted` varchar(255) DEFAULT NULL,
  `CreatedDate` varchar(255) DEFAULT NULL,
  `IsDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BlackListID`)
);
DROP TABLE IF EXISTS `BlackoutListExportTrack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlackoutListExportTrack` (
  `BlackListExportTrackID` int(11) NOT NULL AUTO_INCREMENT,
  `FileName` varchar(255) DEFAULT NULL,
  `ExportDate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BlackListExportTrackID`)
);
DROP TABLE IF EXISTS `CodeTable_ApplicationStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_ApplicationStatus` (
  `ApplicationStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `ApplicationStatusCode` varchar(255) DEFAULT NULL,
  `ApplicationStatus` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ApplicationStatusID`)
);
DROP TABLE IF EXISTS `CodeTable_AuditModuleType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_AuditModuleType` (
  `ModuleID` int(11) NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ModuleID`)
);
DROP TABLE IF EXISTS `CodeTable_AuthorizedPersonnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_AuthorizedPersonnel` (
  `AuthorizedPersonnelID` int(11) NOT NULL AUTO_INCREMENT,
  `OneBankID` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AuthorizedPersonnelID`)
);
DROP TABLE IF EXISTS `CodeTable_BlackoutRestrictionList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_BlackoutRestrictionList` (
  `RestrictionListID` int(11) NOT NULL AUTO_INCREMENT,
  `RestrictionList` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`RestrictionListID`)
);
DROP TABLE IF EXISTS `CodeTable_Broker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_Broker` (
  `BrokerID` int(11) NOT NULL AUTO_INCREMENT,
  `BrokerCode` varchar(255) DEFAULT NULL,
  `Broker` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BrokerID`)
);
DROP TABLE IF EXISTS `CodeTable_Building`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_Building` (
  `BuildingID` int(11) NOT NULL AUTO_INCREMENT,
  `BuildingCode` varchar(255) DEFAULT NULL,
  `Building` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`BuildingID`)
);
DROP TABLE IF EXISTS `CodeTable_Company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_Company` (
  `CompanyID` int(11) NOT NULL AUTO_INCREMENT,
  `Company` varchar(255) DEFAULT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `CompanyDescription` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CompanyID`)
);
DROP TABLE IF EXISTS `CodeTable_Counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_Counter` (
  `CounterID` int(11) NOT NULL AUTO_INCREMENT,
  `CounterCode` varchar(255) DEFAULT NULL,
  `Counter` varchar(255) DEFAULT NULL,
  `StockExchangeID` int(11) DEFAULT NULL,
  `Viewable` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CounterID`)
);
DROP TABLE IF EXISTS `CodeTable_Country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_Country` (
  `CountryID` int(11) NOT NULL AUTO_INCREMENT,
  `CountryCode` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CountryID`)
);
DROP TABLE IF EXISTS `CodeTable_DealType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_DealType` (
  `DealTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `DealTypeCode` varchar(255) DEFAULT NULL,
  `DealType` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DealTypeID`)
);
DROP TABLE IF EXISTS `CodeTable_Dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_Dept` (
  `DeptID` int(11) NOT NULL AUTO_INCREMENT,
  `DeptCode` varchar(255) DEFAULT NULL,
  `Dept` varchar(255) DEFAULT NULL,
  `ParentDeptID` int(11) DEFAULT NULL,
  `CompanyID` int(11) DEFAULT NULL,
  `IsManualUpdated` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DeptID`)
);
DROP TABLE IF EXISTS `CodeTable_StockExchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_StockExchange` (
  `StockExchangeID` int(11) NOT NULL AUTO_INCREMENT,
  `StockExchangeCode` varchar(255) DEFAULT NULL,
  `StockExchange` varchar(255) DEFAULT NULL,
  `CountryCode` varchar(255) DEFAULT NULL,
  `Viewable` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`StockExchangeID`)
);
DROP TABLE IF EXISTS `CodeTable_GreyListStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_GreyListStatus` (
  `GreyListStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `GreyListStatus` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GreyListStatusID`)
);
DROP TABLE IF EXISTS `CodeTable_GroupDept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_GroupDept` (
  `GroupDeptID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupDeptCode` varchar(255) DEFAULT NULL,
  `GroupDept` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GroupDeptID`)
);
DROP TABLE IF EXISTS `CodeTable_NotificationMessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_NotificationMessage` (
  `NotificationID` int(11) NOT NULL AUTO_INCREMENT,
  `NotificationName` varchar(255) DEFAULT NULL,
  `Subject` varchar(255) DEFAULT NULL,
  `Body` text DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`NotificationID`)
);
DROP TABLE IF EXISTS `CodeTable_HolidayList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_HolidayList` (
  `HolidayListID` int(11) NOT NULL AUTO_INCREMENT,
  `StockExchangeCode` varchar(255) DEFAULT NULL,
  `HolidayName` varchar(255) DEFAULT NULL,
  `HolidayDate` varchar(255) DEFAULT NULL,
  `Viewable` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`HolidayListID`)
);
DROP TABLE IF EXISTS `CodeTable_InvestmentType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_InvestmentType` (
  `InvestmentTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `InvestmentTypeCode` varchar(255) DEFAULT NULL,
  `InvestmentType` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`InvestmentTypeID`)
);
DROP TABLE IF EXISTS `CodeTable_OtherGroupMember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_OtherGroupMember` (
  `OtherGrpMemberID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` varchar(255) DEFAULT NULL,
  `OneBankID` INT DEFAULT NULL,
  `Body` text DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  `dateadded` varchar(255) DEFAULT NULL,
  `isgreylistupdated` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OtherGrpMemberID`)
);
DROP TABLE IF EXISTS `CodeTable_OthersGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_OthersGroup` (
  `GroupID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupCode` varchar(255) DEFAULT NULL,
  `GroupName` INT DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GroupID`)
);
DROP TABLE IF EXISTS `CodeTable_PLCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_PLCode` (
  `PLCodeID` int(11) NOT NULL AUTO_INCREMENT,
  `PLCode` varchar(255) DEFAULT NULL,
  `PLCodeDesc` INT DEFAULT NULL,
  `StockExchangeCode` INT DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PLCodeID`)
);
DROP TABLE IF EXISTS `CodeTable_Rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_Rank` (
  `RankID` int(11) NOT NULL AUTO_INCREMENT,
  `RankCode` varchar(255) DEFAULT NULL,
  `Rank` INT DEFAULT NULL,
  `IsManualUpdated` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`RankID`)
);
DROP TABLE IF EXISTS `CodeTable_Relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CodeTable_Relationships` (
  `RelationshipID` int(11) NOT NULL AUTO_INCREMENT,
  `RelationshipCode` varchar(255) DEFAULT NULL,
  `Relationship` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`RelationshipID`)
);
DROP TABLE IF EXISTS `DBS_Vickers_Data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DBS_Vickers_Data` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NRIC` varchar(255) DEFAULT NULL,
  `Name` INT DEFAULT NULL,
  `CounterCode` varchar(255) DEFAULT NULL,
  `CounterName` varchar(255) DEFAULT NULL,
  `Quantity` varchar(255) DEFAULT NULL,
  `Currency` varchar(255) DEFAULT NULL,
  `Price` FLOAT DEFAULT NULL,
  `ValueInSGD` FLOAT DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `TransactionDate` varchar(255) DEFAULT NULL,
  `ForeignExchangeRate` float DEFAULT NULL,
  `StaffCode` varchar(255) DEFAULT NULL,
  `IsException` varchar(255) DEFAULT NULL,
  `ExceptionRemarks` varchar(255) DEFAULT NULL,
  `Department` varchar(255) DEFAULT NULL,
  `Rank` varchar(255) DEFAULT NULL,
  `PIPApprovalDate` varchar(255) DEFAULT NULL,
  `Balance` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedDate` varchar(255) DEFAULT NULL,
  `LastUpdatedBy` varchar(255) DEFAULT NULL,
  `LasteUpdatedDate` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
);
DROP TABLE IF EXISTS `GreyList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GreyList` (
  `GreylistID` int(11) NOT NULL AUTO_INCREMENT,
  `CreationDateTime` varchar(255) DEFAULT NULL,
  `ProjectName` varchar(255) DEFAULT NULL,
  `TeamName` varchar(255) DEFAULT NULL,
  `StatusID` varchar(255) DEFAULT NULL,
  `EffectiveDate` varchar(255) DEFAULT NULL,
  `ExpiryDate` varchar(255) DEFAULT NULL,
  `DealTypeID` varchar(255) DEFAULT NULL,
  `OriginatingRegionID` varchar(255) DEFAULT NULL,
  `OriginatingDeptID` varchar(255) DEFAULT NULL,
  `Reason` varchar(255) DEFAULT NULL,
  `IsDealDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GreylistID`)
);
DROP TABLE IF EXISTS `GreyList_AffectedDept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GreyList_AffectedDept` (
  `GreyListAffectedDeptID` int(11) NOT NULL AUTO_INCREMENT,
  `GreyListID` INT DEFAULT NULL,
  `DepartmentID` INT DEFAULT NULL,
  `IsDealDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GreyListAffectedDeptID`)
);
DROP TABLE IF EXISTS `GreyList_AuthorizedPersonnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GreyList_AuthorizedPersonnel` (
  `GreyListAuthorizedPersonnelID` int(11) NOT NULL AUTO_INCREMENT,
  `GreyListID` INT DEFAULT NULL,
  `AuthorizedPersonnelID` INT DEFAULT NULL,
  `IsDealDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GreyListAuthorizedPersonnelID`)
);
DROP TABLE IF EXISTS `GreyList_OtherMember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GreyList_OtherMember` (
  `GreyListOtherMemberID` int(11) NOT NULL AUTO_INCREMENT,
  `GreyListID` INT DEFAULT NULL,
  `OtherMemberID` INT DEFAULT NULL,
  `IsDealDeleted` varchar(255) DEFAULT NULL,
  `GroupID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GreyListOtherMemberID`)
);
DROP TABLE IF EXISTS `GreyList_Counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GreyList_Counter` (
  `GreyListCounterID` int(11) NOT NULL AUTO_INCREMENT,
  `GreyListID` INT DEFAULT NULL,
  `CounterID` INT DEFAULT NULL,
  `IsDealDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GreyListCounterID`)
);
DROP TABLE IF EXISTS `GreyList_DealTeamMember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GreyList_DealTeamMember` (
  `GreyListDealTeamMemberID` int(11) NOT NULL AUTO_INCREMENT,
  `GreyListID` INT DEFAULT NULL,
  `DealTeamMemberID` INT DEFAULT NULL,
  `IsDealDeleted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GreyListDealTeamMemberID`)
);
DROP TABLE IF EXISTS `NotificationMessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NotificationMessage` (
  `MessageID` int(11) NOT NULL AUTO_INCREMENT,
  `DateTimeToSend` varchar(255) DEFAULT NULL,
  `OneBankID` varchar(255) DEFAULT NULL,
  `Subject` varchar(255) DEFAULT NULL,
  `Body` text DEFAULT NULL,
  `Tries` text DEFAULT NULL,
  `IsSent` text DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  `CC` text DEFAULT NULL,
  PRIMARY KEY (`MessageID`)
);
DROP TABLE IF EXISTS `PIPApplication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PIPApplication` (
  `PIPApplicationID` int(11) NOT NULL AUTO_INCREMENT,
  `CreationDateTime` varchar(255) DEFAULT NULL,
  `StaffID` varchar(255) DEFAULT NULL,
  `TradingParty` varchar(255) DEFAULT NULL,
  `RelationshipID` int DEFAULT NULL,
  `InvestmentTypeID` int DEFAULT NULL,
  `StockExchangeOther` varchar(255) DEFAULT NULL,
  `CounterID` int DEFAULT NULL,
  `OtherCounter` varchar(255) DEFAULT NULL,
  `Quantity` float DEFAULT NULL,
  `SOSScheme` varchar(255) DEFAULT NULL,
  `BrokerID` int DEFAULT NULL,
  `OtherBroker` varchar(255) DEFAULT NULL,
  `IsBuy` varchar(255) DEFAULT NULL,
  `StatusID` int DEFAULT NULL,
  `IsManualEntry` varchar(100) DEFAULT NULL,
  `IsNotified` varchar(100) DEFAULT NULL,
  `MessageID` int DEFAULT NULL,
  `SelectedApproverID` int DEFAULT NULL,
  `NextApproverID` int DEFAULT NULL,
  `ApprovalDateTime` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PIPApplicationID`)
);
DROP TABLE IF EXISTS `PIPApplication_Approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PIPApplication_Approval` (
  `PIPApplicationID` int(11) NOT NULL AUTO_INCREMENT,
  `ApproverID` int DEFAULT NULL,
  `StatusID` int DEFAULT NULL,
  `CreationDateTime` varchar(255) DEFAULT NULL,
  `ApprovalDateTime` varchar(255) DEFAULT NULL,
  `Reason` varchar(255) DEFAULT NULL,
  `IsApproved` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PIPApplicationID`)
);
DROP TABLE IF EXISTS `PIP_AdditionalApprovalExcludeExchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PIP_AdditionalApprovalExcludeExchange` (
  `ExchangeID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ExchangeID`)
);
DROP TABLE IF EXISTS `RelatedRoleTask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelatedRoleTask` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `TaskID` int DEFAULT NULL,
  PRIMARY KEY (`RoleID`)
);
DROP TABLE IF EXISTS `RelatedUserRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RelatedUserRole` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `RoleID` int DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
);
DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`RoleID`)
);
DROP TABLE IF EXISTS `ServiceMethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceMethod` (
  `MethodID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `DisplayName` varchar(255) DEFAULT NULL,
  `ModuleID` int DEFAULT NULL,
  `ActionID` int DEFAULT NULL,
  `Auditable` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MethodID`)
);
DROP TABLE IF EXISTS `StaffProfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StaffProfile` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `StaffID` int DEFAULT NULL,
  `NationalID` varchar(255) DEFAULT NULL,
  `OneBankID` varchar(255) DEFAULT NULL,
  `CompanyID` int DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `NamePrefix` varchar(255) DEFAULT NULL,
  `NationalIDName` varchar(255) DEFAULT NULL,
  `CountryID` varchar(255) DEFAULT NULL,
  `BuildingID` int DEFAULT NULL,
  `RankID` int DEFAULT NULL,
  `GroupDeptID` int DEFAULT NULL,
  `DeptID` int DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `SupervisorOneBankID` varchar(255) DEFAULT NULL,
  `AlternateSupervisorOneBankID` varchar(255) DEFAULT NULL,
  `DeclaredDate` varchar(255) DEFAULT NULL,
  `JoinDate` varchar(255) DEFAULT NULL,
  `Covered` varchar(255) DEFAULT NULL,
  `SupervisorApprovalRequired` varchar(255) DEFAULT NULL,
  `AdditionalApprovalRequired` varchar(255) DEFAULT NULL,
  `ComplianceApprovalRequired` varchar(255) DEFAULT NULL,
  `Office` varchar(255) DEFAULT NULL,
  `IsDeleted` varchar(255) DEFAULT NULL,
  `IsManualUpdated` varchar(255) DEFAULT NULL,
  `InActiveDate` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `SupervisorUpdatedbyOneBankid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
);
DROP TABLE IF EXISTS `StaffTrades_Vickers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StaffTrades_Vickers` (
  `TradeID` int(11) NOT NULL AUTO_INCREMENT,
  `OneBankID` varchar(255) DEFAULT NULL,
  `NationalID` varchar(255) DEFAULT NULL,
  `CLINME` varchar(255) DEFAULT NULL,
  `CLINO` int DEFAULT NULL,
  `CONTDT` varchar(255) DEFAULT NULL,
  `SECME` varchar(255) DEFAULT NULL,
  `CONTNO` varchar(255) DEFAULT NULL,
  `IsBuy` varchar(255) DEFAULT NULL,
  `Quantity` float DEFAULT NULL,
  `Price` float DEFAULT NULL,
  `NetAmt` decimal DEFAULT NULL,
  `SETLCU` decimal DEFAULT NULL,
  `SECCD` decimal DEFAULT NULL,
  `UploadDate` varchar(255) DEFAULT NULL,
  `CountryID` int DEFAULT NULL,
  `CompanyID` int DEFAULT NULL,
  PRIMARY KEY (`TradeID`)
);
DROP TABLE IF EXISTS `StaffTrades_VickersOnline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StaffTrades_VickersOnline` (
  `TradeID` int(11) NOT NULL AUTO_INCREMENT,
  `OneBankID` varchar(255) DEFAULT NULL,
  `ClientID` varchar(255) DEFAULT NULL,
  `SubAC` varchar(255) DEFAULT NULL,
  `ClientName` int DEFAULT NULL,
  `NationalID` varchar(255) DEFAULT NULL,
  `IsBuy` varchar(255) DEFAULT NULL,
  `ContractNo` varchar(255) DEFAULT NULL,
  `PLCode` varchar(255) DEFAULT NULL,
  `TradeDate` float DEFAULT NULL,
  `SettDate` float DEFAULT NULL,
  `StockCode` decimal DEFAULT NULL,
  `Price` decimal DEFAULT NULL,
  `Quantity` decimal DEFAULT NULL,
  `SettCCY` varchar(255) DEFAULT NULL,
  `SettAmt` decimal DEFAULT NULL,
  `UploadDate` varchar(255) DEFAULT NULL,
  `CountryID` int DEFAULT NULL,
  `CompanyID` int DEFAULT NULL,
  PRIMARY KEY (`TradeID`)
);
DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `ActionID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ActionID`)
);
