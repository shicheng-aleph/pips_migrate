-- MySQL dump 10.13  Distrib 5.5.33, for Linux (x86_64)
--
-- Host: 175.41.169.176    Database: pips
-- ------------------------------------------------------
-- Server version	5.5.31-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `cc_question`
--

LOCK TABLES `cc_question` WRITE;
/*!40000 ALTER TABLE `cc_question` DISABLE KEYS */;
INSERT INTO `cc_question` VALUES (1,'Does DBS Group or any of its Directors or any Deal Team member or  Financial Dependents of any Deal Team member have any shareholding/interest in the client or its affiliates?',1),(2,'Any of DBS Group\'s Directors or any Deal Team member or immediate Family of any Deal Team member are executive officers or on the Board of Directors of the client or its affiliates?',1),(3,'Details of any Deal Team member\'s personal service aggrements with the client or its affiliates?',1),(4,'Any credit facilities granted?',1),(5,'Does DBS Group or any of its Directors or any Deal Team member or  Financial Dependents of any Deal Team member have any shareholding/interest in the opposing party or its affiliates?',2),(6,'Any of DBS Group\'s Directors or any Deal Team member or immediate Family of any Deal Team member are executive officers or on the Board of Directors of the opposing party or its affiliates?',2),(7,'Details of any Deal Team member\'s personal service aggrements with the opposing party or its affiliates?',2),(8,'Any credit facilities granted?',2),(9,'Does DBS Group or any of its Directors or any Deal Team member or  Financial Dependents of any Deal Team member have any shareholding/interest in any other parties?',3),(10,'Any of DBS Group\'s Directors or any Deal Team member or immediate Family of any Deal Team member are executive officers or on the Board of Directors of any other parties?',3),(11,'Details of any Deal Team member\'s personal service engagements with any other parties? ',3),(12,'Any credit facilities granted?',3);
/*!40000 ALTER TABLE `cc_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cc_question_answer`
--

LOCK TABLES `cc_question_answer` WRITE;
/*!40000 ALTER TABLE `cc_question_answer` DISABLE KEYS */;
INSERT INTO `cc_question_answer` VALUES (25,'yes',NULL,3,1),(26,'no',NULL,3,2),(27,'yes',NULL,3,3),(28,'yes',NULL,3,4),(29,'yes',NULL,3,5),(30,'no',NULL,3,6),(31,'yes',NULL,3,7),(32,'no',NULL,3,8),(33,'yes',NULL,3,9),(34,'no',NULL,3,10),(35,'yes',NULL,3,11),(36,'no',NULL,3,12),(37,'yes',NULL,4,1),(38,'no',NULL,4,2),(39,'no',NULL,4,3),(40,'yes',NULL,4,4),(41,'yes',NULL,4,5),(42,'yes',NULL,4,6),(43,'no',NULL,4,7),(44,'no',NULL,4,8),(45,'yes',NULL,4,9),(46,'no',NULL,4,10),(47,'no',NULL,4,11),(48,'NA',NULL,4,12),(49,'yes',NULL,5,1),(50,'no',NULL,5,2),(51,'yes',NULL,5,3),(52,'no',NULL,5,4),(53,'yes',NULL,5,5),(54,'no',NULL,5,6),(55,'yes',NULL,5,7),(56,'no',NULL,5,8),(57,'no',NULL,5,9),(58,'no',NULL,5,10),(59,'no',NULL,5,11),(60,'no',NULL,5,12),(61,'yes',NULL,6,1),(62,'no',NULL,6,2),(63,'NA',NULL,6,3),(64,'NA',NULL,6,4),(65,'no',NULL,6,5),(66,'yes',NULL,6,6),(67,'no',NULL,6,7),(68,'no',NULL,6,8),(69,'no',NULL,6,9),(70,'yes',NULL,6,10),(71,'yes',NULL,6,11),(72,'no',NULL,6,12),(73,'yes',NULL,7,1),(74,'yes',NULL,7,2),(75,'no',NULL,7,3),(76,'yes',NULL,7,4),(77,'yes',NULL,7,5),(78,'no',NULL,7,6),(79,'yes',NULL,7,7),(80,'no',NULL,7,8),(81,'yes',NULL,7,9),(82,'no',NULL,7,10),(83,'yes',NULL,7,11),(84,'yes',NULL,7,12),(85,NULL,NULL,8,1),(86,NULL,NULL,8,2),(87,NULL,NULL,8,3),(88,NULL,NULL,8,4),(89,NULL,NULL,8,5),(90,NULL,NULL,8,6),(91,NULL,NULL,8,7),(92,NULL,NULL,8,8),(93,NULL,NULL,8,9),(94,NULL,NULL,8,10),(95,NULL,NULL,8,11),(96,NULL,NULL,8,12),(97,NULL,NULL,9,1),(98,NULL,NULL,9,2),(99,NULL,NULL,9,3),(100,NULL,NULL,9,4),(101,NULL,NULL,9,5),(102,NULL,NULL,9,6),(103,NULL,NULL,9,7),(104,NULL,NULL,9,8),(105,NULL,NULL,9,9),(106,NULL,NULL,9,10),(107,NULL,NULL,9,11),(108,NULL,NULL,9,12),(109,NULL,NULL,10,1),(110,NULL,NULL,10,2),(111,NULL,NULL,10,3),(112,NULL,NULL,10,4),(113,NULL,NULL,10,5),(114,NULL,NULL,10,6),(115,NULL,NULL,10,7),(116,NULL,NULL,10,8),(117,NULL,NULL,10,9),(118,NULL,NULL,10,10),(119,NULL,NULL,10,11),(120,NULL,NULL,10,12),(121,NULL,NULL,11,1),(122,NULL,NULL,11,2),(123,NULL,NULL,11,3),(124,NULL,NULL,11,4),(125,NULL,NULL,11,5),(126,NULL,NULL,11,6),(127,NULL,NULL,11,7),(128,NULL,NULL,11,8),(129,NULL,NULL,11,9),(130,NULL,NULL,11,10),(131,NULL,NULL,11,11),(132,NULL,NULL,11,12),(133,'yes',NULL,12,1),(134,'no',NULL,12,2),(135,'NA',NULL,12,3),(136,'NA',NULL,12,4),(137,'yes',NULL,12,5),(138,'no',NULL,12,6),(139,'NA',NULL,12,7),(140,'NA',NULL,12,8),(141,'yes',NULL,12,9),(142,'yes',NULL,12,10),(143,'yes',NULL,12,11),(144,'yes',NULL,12,12),(145,NULL,NULL,13,1),(146,NULL,NULL,13,2),(147,NULL,NULL,13,3),(148,NULL,NULL,13,4),(149,NULL,NULL,13,5),(150,NULL,NULL,13,6),(151,NULL,NULL,13,7),(152,NULL,NULL,13,8),(153,NULL,NULL,13,9),(154,NULL,NULL,13,10),(155,NULL,NULL,13,11),(156,NULL,NULL,13,12),(157,NULL,NULL,14,1),(158,NULL,NULL,14,2),(159,NULL,NULL,14,3),(160,NULL,NULL,14,4),(161,NULL,NULL,14,5),(162,NULL,NULL,14,6),(163,NULL,NULL,14,7),(164,NULL,NULL,14,8),(165,NULL,NULL,14,9),(166,NULL,NULL,14,10),(167,NULL,NULL,14,11),(168,NULL,NULL,14,12),(169,'yes',NULL,15,1),(170,'NA',NULL,15,2),(171,'NA',NULL,15,3),(172,'NA',NULL,15,4),(173,'no',NULL,15,5),(174,'NA',NULL,15,6),(175,'NA',NULL,15,7),(176,'NA',NULL,15,8),(177,'NA',NULL,15,9),(178,'NA',NULL,15,10),(179,'NA',NULL,15,11),(180,'NA',NULL,15,12),(181,'no',NULL,16,1),(182,'NA',NULL,16,2),(183,'NA',NULL,16,3),(184,'NA',NULL,16,4),(185,'no',NULL,16,5),(186,'NA',NULL,16,6),(187,'NA',NULL,16,7),(188,'NA',NULL,16,8),(189,'NA',NULL,16,9),(190,'no',NULL,16,10),(191,'no',NULL,16,11),(192,'no',NULL,16,12),(193,'yes',NULL,17,1),(194,'yes',NULL,17,2),(195,'yes',NULL,17,3),(196,'yes',NULL,17,4),(197,'yes',NULL,17,5),(198,'yes',NULL,17,6),(199,'yes',NULL,17,7),(200,'yes',NULL,17,8),(201,'yes',NULL,17,9),(202,'yes',NULL,17,10),(203,'yes',NULL,17,11),(204,'yes',NULL,17,12),(205,'yes',NULL,18,1),(206,'yes',NULL,18,2),(207,'yes',NULL,18,3),(208,'yes',NULL,18,4),(209,'yes',NULL,18,5),(210,'yes',NULL,18,6),(211,'yes',NULL,18,7),(212,'yes',NULL,18,8),(213,'yes',NULL,18,9),(214,'yes',NULL,18,10),(215,'yes',NULL,18,11),(216,'yes',NULL,18,12),(217,'yes',NULL,19,1),(218,'yes',NULL,19,2),(219,'yes',NULL,19,3),(220,'yes',NULL,19,4),(221,'yes',NULL,19,5),(222,'yes',NULL,19,6),(223,'yes',NULL,19,7),(224,'yes',NULL,19,8),(225,'yes',NULL,19,9),(226,'yes',NULL,19,10),(227,'yes',NULL,19,11),(228,'yes',NULL,19,12),(229,NULL,NULL,20,1),(230,NULL,NULL,20,2),(231,NULL,NULL,20,3),(232,NULL,NULL,20,4),(233,NULL,NULL,20,5),(234,NULL,NULL,20,6),(235,NULL,NULL,20,7),(236,NULL,NULL,20,8),(237,NULL,NULL,20,9),(238,NULL,NULL,20,10),(239,NULL,NULL,20,11),(240,NULL,NULL,20,12),(241,'NA',NULL,21,1),(242,'no',NULL,21,2),(243,'no',NULL,21,3),(244,'NA',NULL,21,4),(245,'NA',NULL,21,5),(246,'no',NULL,21,6),(247,'no',NULL,21,7),(248,'no',NULL,21,8),(249,'NA',NULL,21,9),(250,'NA',NULL,21,10),(251,'NA',NULL,21,11),(252,'NA',NULL,21,12),(253,NULL,NULL,22,1),(254,NULL,NULL,22,2),(255,NULL,NULL,22,3),(256,NULL,NULL,22,4),(257,NULL,NULL,22,5),(258,NULL,NULL,22,6),(259,NULL,NULL,22,7),(260,NULL,NULL,22,8),(261,NULL,NULL,22,9),(262,NULL,NULL,22,10),(263,NULL,NULL,22,11),(264,NULL,NULL,22,12),(265,NULL,NULL,23,1),(266,NULL,NULL,23,2),(267,NULL,NULL,23,3),(268,NULL,NULL,23,4),(269,NULL,NULL,23,5),(270,NULL,NULL,23,6),(271,NULL,NULL,23,7),(272,NULL,NULL,23,8),(273,NULL,NULL,23,9),(274,NULL,NULL,23,10),(275,NULL,NULL,23,11),(276,NULL,NULL,23,12),(277,'no',NULL,24,1),(278,'no',NULL,24,2),(279,'no',NULL,24,3),(280,'no',NULL,24,4),(281,'no',NULL,24,5),(282,'no',NULL,24,6),(283,'no',NULL,24,7),(284,'no',NULL,24,8),(285,'no',NULL,24,9),(286,'no',NULL,24,10),(287,'no',NULL,24,11),(288,'no',NULL,24,12),(289,NULL,NULL,25,1),(290,NULL,NULL,25,2),(291,NULL,NULL,25,3),(292,NULL,NULL,25,4),(293,NULL,NULL,25,5),(294,NULL,NULL,25,6),(295,NULL,NULL,25,7),(296,NULL,NULL,25,8),(297,NULL,NULL,25,9),(298,NULL,NULL,25,10),(299,NULL,NULL,25,11),(300,NULL,NULL,25,12),(301,NULL,NULL,26,1),(302,NULL,NULL,26,2),(303,NULL,NULL,26,3),(304,NULL,NULL,26,4),(305,NULL,NULL,26,5),(306,NULL,NULL,26,6),(307,NULL,NULL,26,7),(308,NULL,NULL,26,8),(309,NULL,NULL,26,9),(310,NULL,NULL,26,10),(311,NULL,NULL,26,11),(312,NULL,NULL,26,12),(313,NULL,NULL,27,1),(314,NULL,NULL,27,2),(315,NULL,NULL,27,3),(316,NULL,NULL,27,4),(317,NULL,NULL,27,5),(318,NULL,NULL,27,6),(319,NULL,NULL,27,7),(320,NULL,NULL,27,8),(321,NULL,NULL,27,9),(322,NULL,NULL,27,10),(323,NULL,NULL,27,11),(324,NULL,NULL,27,12),(325,NULL,NULL,28,1),(326,NULL,NULL,28,2),(327,NULL,NULL,28,3),(328,NULL,NULL,28,4),(329,NULL,NULL,28,5),(330,NULL,NULL,28,6),(331,NULL,NULL,28,7),(332,NULL,NULL,28,8),(333,NULL,NULL,28,9),(334,NULL,NULL,28,10),(335,NULL,NULL,28,11),(336,NULL,NULL,28,12),(337,NULL,NULL,29,1),(338,NULL,NULL,29,2),(339,NULL,NULL,29,3),(340,NULL,NULL,29,4),(341,NULL,NULL,29,5),(342,NULL,NULL,29,6),(343,NULL,NULL,29,7),(344,NULL,NULL,29,8),(345,NULL,NULL,29,9),(346,NULL,NULL,29,10),(347,NULL,NULL,29,11),(348,NULL,NULL,29,12),(349,'no',NULL,30,1),(350,'no',NULL,30,2),(351,'yes',NULL,30,3),(352,'no',NULL,30,4),(353,'yes',NULL,30,5),(354,'yes',NULL,30,6),(355,'yes',NULL,30,7),(356,'yes',NULL,30,8),(357,'yes',NULL,30,9),(358,'no',NULL,30,10),(359,'no',NULL,30,11),(360,'no',NULL,30,12),(361,'yes',NULL,31,1),(362,NULL,NULL,31,2),(363,'NA',NULL,31,3),(364,'NA',NULL,31,4),(365,'NA',NULL,31,5),(366,'no',NULL,31,6),(367,'yes',NULL,31,7),(368,'yes',NULL,31,8),(369,'no',NULL,31,9),(370,'no',NULL,31,10),(371,'no',NULL,31,11),(372,'yes',NULL,31,12),(373,NULL,NULL,32,1),(374,NULL,NULL,32,2),(375,NULL,NULL,32,3),(376,NULL,NULL,32,4),(377,NULL,NULL,32,5),(378,NULL,NULL,32,6),(379,NULL,NULL,32,7),(380,NULL,NULL,32,8),(381,NULL,NULL,32,9),(382,NULL,NULL,32,10),(383,NULL,NULL,32,11),(384,NULL,NULL,32,12),(385,NULL,NULL,33,1),(386,NULL,NULL,33,2),(387,NULL,NULL,33,3),(388,NULL,NULL,33,4),(389,NULL,NULL,33,5),(390,NULL,NULL,33,6),(391,NULL,NULL,33,7),(392,NULL,NULL,33,8),(393,NULL,NULL,33,9),(394,NULL,NULL,33,10),(395,NULL,NULL,33,11),(396,NULL,NULL,33,12),(397,NULL,NULL,34,1),(398,NULL,NULL,34,2),(399,NULL,NULL,34,3),(400,NULL,NULL,34,4),(401,NULL,NULL,34,5),(402,NULL,NULL,34,6),(403,NULL,NULL,34,7),(404,NULL,NULL,34,8),(405,NULL,NULL,34,9),(406,NULL,NULL,34,10),(407,NULL,NULL,34,11),(408,NULL,NULL,34,12);
/*!40000 ALTER TABLE `cc_question_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `cc_question_category`
--

LOCK TABLES `cc_question_category` WRITE;
/*!40000 ALTER TABLE `cc_question_category` DISABLE KEYS */;
INSERT INTO `cc_question_category` VALUES (1,'clientAffiliates'),(2,'opposingPartyAffiliates'),(3,'otherParties');
/*!40000 ALTER TABLE `cc_question_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `pips_app`
--

LOCK TABLES `pips_app` WRITE;
/*!40000 ALTER TABLE `pips_app` DISABLE KEYS */;
INSERT INTO `pips_app` VALUES (1,'PIPS-CONTROLROOM',1),(2,'PIPS-EMPLOYEE',1);
/*!40000 ALTER TABLE `pips_app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `pips_group`
--

LOCK TABLES `pips_group` WRITE;
/*!40000 ALTER TABLE `pips_group` DISABLE KEYS */;
INSERT INTO `pips_group` VALUES (1,'Power Admin',1,1),(2,'Super Admin',1,1),(3,'Country Compliance',1,1),(4,'Special Employee',1,2);
/*!40000 ALTER TABLE `pips_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `pips_role`
--

LOCK TABLES `pips_role` WRITE;
/*!40000 ALTER TABLE `pips_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `pips_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-23  8:34:37
