CSV_PATH="../old_pips_db"
MYSQL_USER="root"
MYSQL_PASS="password"
echo "importingi A_BlackoutList.csv"
sed -i -e 's///g' "${CSV_PATH}/A_BlackoutList.csv"
sed -i -e 's/[^[:print:]]//g' "${CSV_PATH}/A_BlackoutList.csv"
tr -s '\n' '' < "${CSV_PATH}/A_BlackoutList.csv" | tee "${CSV_PATH}/A_BlackoutList.clean.csv"
sed -i -e 's/\]/\]==br==/g' "${CSV_PATH}/A_BlackoutList.clean.csv"
tr -s '' '\n' < "${CSV_PATH}/A_BlackoutList.clean.csv" | tee "${CSV_PATH}/A_BlackoutList.csv"
mysql -u${MYSQL_USER} -p${MYSQL_PASS} --execute="LOAD DATA LOCAL INFILE '${CSV_PATH}/A_BlackoutList.csv' INTO TABLE  old_pips.A_BlackoutList  FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n' IGNORE 1 LINES; SHOW WARNINGS"

