MYSQL_USER="root"
MYSQL_PASS="password"
DATA_PATH="../data"
DATABASE="old_pips"
cd $DATA_PATH
DATA_PATH=`pwd`

echo "migrating country"
mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "SELECT CountryID, CountryCode,Country,Active FROM ${DATABASE}.CodeTable_Country;" | sed 's/	//g' |\
while IFS=''  read CountryID CountryCode Country Active ; do
    echo "INSERT into country (id_country,iso2_code,name) VALUES \
         (${CountryID},'${CountryCode}','${Country}');"
done |  tee "${DATA_PATH}/country.data"
echo "migrating exchange"
mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "SELECT ex.StockExchangeID, ex.StockExchangeCode,ex.StockExchange,c.CountryID,ex.Active FROM ${DATABASE}.CodeTable_StockExchange ex, \
      ${DATABASE}.CodeTable_Country c where c.CountryCode = ex.CountryCode;" \
| sed 's/	//g' |\
while IFS=''  read StockExchangeID StockExchangeCode StockExchange CountryID Active ; do
    echo "INSERT into stock_exchange (id_stock_exchange,created_date,is_active,last_modified_date,code,name,fk_country) VALUES \
         (${StockExchangeID},now(),1,now(),'${StockExchangeCode}','${StockExchange}',${CountryID});"
done |  tee "${DATA_PATH}/exchange.data"

echo "migrating counter"
mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "SELECT CounterID, CounterCode,Counter,StockExchangeID,Active FROM ${DATABASE}.CodeTable_Counter;" | sed 's/	//g' \
| sed "s/'/''/g" |\
while IFS=''  read CounterID CounterCode Counter StockExchangeID Active ; do
    echo "INSERT into counter (id_counter,created_date,is_active,last_modified_date,code,name,fk_stock_exchange) VALUES \
         (${CounterID},now(),1,now(),'${CounterCode}','${Counter}',${StockExchangeID});"
done |  tee "${DATA_PATH}/counter.data"

