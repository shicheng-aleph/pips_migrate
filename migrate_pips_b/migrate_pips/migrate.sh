MYSQL_USER="root"
MYSQL_PASS="password"
DATA_PATH="../data"
SQL_PATH="../sql"
DATABASE="old_pips"
cd $DATA_PATH
DATA_PATH=`pwd`

migrate_investment_type(){
   echo "migrating investment type"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "SELECT InvestmentTypeID,InvestmentTypeCode,InvestmentType,Active FROM ${DATABASE}.CodeTable_InvestmentType where Active = 'True';" | sed 's/	//g' |\
   while IFS=''  read InvestmentTypeID InvestmentTypeCode InvestmentType Active ; do
       echo "INSERT into investment_type (id_investment_type,name) VALUES \
            (${InvestmentTypeID},'${InvestmentType}');"
   done |  tee "${DATA_PATH}/investment_type.data"
}

migrate_country(){
   echo "migrating country"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "SELECT CountryID, CountryCode,Country,Active FROM ${DATABASE}.CodeTable_Country;" | sed 's/	//g' |\
   while IFS=''  read CountryID CountryCode Country Active ; do
       echo "INSERT into country (id_country,iso2_code,name) VALUES \
            (${CountryID},'${CountryCode}','${Country}');"
   done |  tee "${DATA_PATH}/country.data"
}
migrate_exchange(){
   echo "migrating exchange"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "SELECT ex.StockExchangeID, ex.StockExchangeCode,ex.StockExchange,c.CountryID,ex.Active FROM ${DATABASE}.CodeTable_StockExchange ex, \
         ${DATABASE}.CodeTable_Country c where c.CountryCode = ex.CountryCode and ex.Active = 'True';" \
   | sed 's/	//g' |\
   while IFS=''  read StockExchangeID StockExchangeCode StockExchange CountryID Active ; do
       echo "INSERT into stock_exchange (id_stock_exchange,created_date,is_active,last_modified_date,code,name,fk_country) VALUES \
         (${StockExchangeID},now(),1,now(),'${StockExchangeCode}','${StockExchange}',${CountryID});"
   done |  tee "${DATA_PATH}/exchange.data"
}
migrate_counter(){
   echo "migrating counter"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "SELECT CounterID, CounterCode,Counter,StockExchangeID,Active FROM ${DATABASE}.CodeTable_Counter where Active = 'True';" | sed 's/	//g' \
   | sed "s/'/''/g" |\
   while IFS=''  read CounterID CounterCode Counter StockExchangeID Active ; do
       echo "INSERT into counter (id_counter,created_date,is_active,last_modified_date,code,name,fk_stock_exchange) VALUES \
            (${CounterID},now(),1,now(),'${CounterCode}','${Counter}',${StockExchangeID});"
   done |  tee "${DATA_PATH}/counter.data"
}
migrate_broker(){
   echo "migrating broker"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "SELECT BrokerID, BrokerCode,Broker,Active FROM ${DATABASE}.CodeTable_Broker where Active like '%True%';" | sed 's/	//g' \
   | sed "s/'/''/g" |\
   while IFS=''  read BrokerID BrokerCode Broker Active ; do
       echo "INSERT into broker (id_broker,created_date,is_active,last_modified_date,code,name,fk_user_created,fk_user_last_modified,is_external) VALUES \
            (${BrokerID},now(),1,now(),'${BrokerCode}','${Broker}',null,null,0);"
   done |  tee "${DATA_PATH}/broker.data"
}
migrate_holiday_list(){
   echo "migrating holiday list"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} -N < "${SQL_PATH}/select_holiday.sql" | sed 's/	//g' \
   | sed "s/'/''/g" |\
   while IFS=''  read HolidayListID StockExchangeID HolidayName HolidayDate Active ; do
       echo "INSERT into public_holiday (id_public_holiday,created_date,is_active,last_modified_date,description,holiday_date,fk_user_created,fk_user_last_modified,fk_stock_exchange) VALUES \
            (${HolidayListID},now(),1,now(),'${HolidayName}','${HolidayDate}',null,null,${StockExchangeID});"
   done |  tee "${DATA_PATH}/holiday.data"
}
migrate_department(){
   echo "migrating department"
   
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} -N < "${SQL_PATH}/select_department.sql" | sed 's/	//g' \
   | sed "s/'/''/g" | \
   while IFS=''  read deptId deptCode deptDesc countryID company level4code level4desc level5code level5desc ; do
       echo "INSERT into department (id_department,created_date,is_active,last_modified_date,code,name,fk_country,company,org_structure_level_4,org_structure_level_4_desc,org_structure_level_5,org_structure_level_5_desc) VALUES \
         (${deptId},now(),1,now(),'${deptCode}','${deptDesc}',${countryID},'${company}','${level4code}','${level4desc}','${level5code}','${level5desc}');"
   done |  tee "${DATA_PATH}/department.data"
}
migrate_blackout_list(){
   echo "migrating blackout list"
   
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} -N < "${SQL_PATH}/select_blackout_list.sql" | sed 's/	//g' \
   | sed "s/'/''/g" | \
   while IFS=''  read BlackListID CreatedDate Comments CSStartDate CSEndDate PIPStartDate PIPEndDate PTStartDate PTEndDate Remarks ResearchStartDate ResearchEndDate Status CounterID; do
       echo "INSERT into blackout_list (id_blackout_list,created_date,is_active,last_modified_date,client_solicitation_end_date,client_solicitation_start_date, \
             personal_investment_policy_end_date,personal_investment_policy_start_date,proprietary_trading_end_date,proprietary_trading_start_date,remarks, \
             research_end_date,research_start_date,status,fk_user_created,fk_user_last_modified,fk_counter) VALUES \
             (${BlackListID},'${CreatedDate}',1,now(),'${Comments}','${CSEndDate}','${CSStartDate}','${PIPEndDate}','${PIPStartDate}','${PTEndDate}','${PTStartDate}','${Remarks}', \
             '${ResearchEndDate}','${ResearchStartDate}',null,null,${CounterID});"
   done |  tee "${DATA_PATH}/blackout_list.data"
}
migrate_audit_blackout_list(){
   echo "migrating blackout list audit"
   
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} -N < "${SQL_PATH}/select_blackout_list_audit.sql" | sed 's/	//g' \
   | sed "s/'/''/g" | \
   while IFS=''  read AID BlackListID CreatedDate Comments CSStartDate CSEndDate PIPStartDate PIPEndDate PTStartDate PTEndDate Remarks ResearchStartDate ResearchEndDate Status CounterID REVISIONNO; do
       echo "INSERT into blackout_list_copy (id_blackout_list_copy,created_date,is_active,last_modified_date,client_solicitation_end_date,client_solicitation_start_date, \
             personal_investment_policy_end_date,personal_investment_policy_start_date,proprietary_trading_end_date,proprietary_trading_start_date,remarks, \
             research_end_date,research_start_date,status,fk_user_created,fk_user_last_modified,fk_counter,fk_blackout_list,revision_no) VALUES \
             (${AID},'${CreatedDate}',1,now(),'${Comments}','${CSEndDate}','${CSStartDate}','${PIPEndDate}','${PIPStartDate}','${PTEndDate}','${PTStartDate}','${Remarks}', \
             '${ResearchEndDate}','${ResearchStartDate}',null,null,${CounterID},${BlackListID},${REVISIONNO});"
   done |  tee "${DATA_PATH}/blackout_list_audit.data"
}

migrate_staff(){
   echo "migrating staff profile to ${DATA_PATH}/user.data"
mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "select ID,OneBankID,CountryID,DeptID,NationalID,IF(Covered='True',1,0) from ${DATABASE}.StaffProfile where IsDeleted = 'False'; " > "${DATA_PATH}/test.log"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "select ID,OneBankID,CountryID,DeptID,NationalID,IF(Covered='True',1,0) from ${DATABASE}.StaffProfile where IsDeleted != 'True';" | sed 's/	//g' \
   | sed "s/'/''/g" |\
   while IFS=''  read StaffID OneBankID CountryID DeptID NationalID Covered ; do
       echo "INSERT into user (id_user,created_date,is_active,last_modified_date,bank_id,fk_user_created,fk_user_last_modified,fk_country,fk_department,nric,is_covered, \
            has_declared,last_notified_date,covered_date,cr_roles_deactivated_date) VALUES \
            (${StaffID},now(),1,now(),'${OneBankID}',null,null,${CountryID},${DeptID},'${NationalID}',${Covered},0,null,null,null);"
   done |  tee "${DATA_PATH}/user.data"
}
migrate_greylist(){
   echo "migrating greylist"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} ${DATABASE} -N < "${SQL_PATH}/select_greylist.sql" | sed 's/	//g' \
   | sed "s/'/''/g" |\
   while IFS=''  read GreylistID CreationDateTime ExpiryDate ProjectName Reason EffectiveDate GreyListStatus ; do
       echo "INSERT into grey_list (id_grey_list,created_date,is_active,last_modified_date,expiry_date,project_name,reason,start_date,status,fk_user_created,fk_user_last_modified, \
            parent_grey_list_id,reject_reason,is_expire_now,is_new,revision_no) VALUES \
            (${GreylistID},now(),1,now(),${ExpiryDate},'${ProjectName}','${Reason}',${EffectiveDate},'${GreyListStatus}',null,null,null,null,null,null);"
   done |  tee "${DATA_PATH}/greylist.data"
}
migrate_greylist_affected_department(){
   echo "migrating greylist affected department"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "select GreyListAffectedDeptID,GreyListID,DepartmentID from ${DATABASE}.GreyList_AffectedDept where IsDeleted = 'False';" | sed 's/	//g' \
   | sed "s/'/''/g" |\
   while IFS=''  read GreyListAffectedDeptID GreyListID DepartmentID ; do
       echo "INSERT into grey_list_department (id_grey_list_department,created_date,is_active,last_modified_date,fk_user_created, \
             fk_user_last_modified,fk_department,fk_grey_list) VALUES \
            (${GreyListAffectedDeptID},now(),1,now(),null,null,${DepartmentID},${GreyListID});"
   done |  tee "${DATA_PATH}/greylist_department.data"
}
migrate_greylist_share_counter(){
   echo "migrating greylist share counter"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "select GreyListCounterID,GreyListID,CounterID from ${DATABASE}.GreyList_Counter where IsDeleted = 'False';" | sed 's/	//g' \
   | sed "s/'/''/g" |\
   while IFS=''  read GreyListCounterID GreyListID CounterID ; do
       echo "INSERT into grey_list_share_counter (id_grey_list_counter,created_date,is_active,last_modified_date,fk_user_created, \
             fk_user_last_modified,fk_counter,fk_grey_list) VALUES \
            (${GreyListCounterID},now(),1,now(),null,null,${CounterID},${GreyListID});"
   done |  tee "${DATA_PATH}/greylist_share_counter.data"
}
migrate_greylist_deal_team_member(){
   echo "migrating deal team member"
   mysql -u${MYSQL_USER} -p${MYSQL_PASS} -N -e "select GreyListDealTeamMemberID,GreyListID,DealTeamMemberID from ${DATABASE}.GreyList_DealTeamMember where IsDeleted = 'False';" | sed 's/	//g' \
   | sed "s/'/''/g" |\
   while IFS=''  read GreyListDealTeamMemberID GreyListID DealTeamMemberID ; do
       echo "INSERT into deal_team_member (id_deal_team_member,created_date,is_active,last_modified_date,fk_user_created, \
             fk_user_last_modified,fk_user,fk_grey_list) VALUES \
            (${GreyListDealTeamMemberID},now(),1,now(),null,null,${DealTeamMemberID},${GreyListID});"
   done |  tee "${DATA_PATH}/deal_team_member.data"
}
#migrate_country
migrate_investment_type
#migrate_exchange
#migrate_counter
#migrate_broker
#migrate_holiday_list
#migrate_department
#migrate_staff
#migrate_greylist
#migrate_greylist_affected_department
#migrate_greylist_share_counter
#migrate_greylist_deal_team_member
#migrate_blackout_list
#migrate_audit_blackout_list
