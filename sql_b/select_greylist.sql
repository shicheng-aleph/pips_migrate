select gl.GreylistID,gl.CreationDateTime,gl.ExpiryDate,gl.ProjectName,gl.Reason,gl.EffectiveDate,IF(cgs.GreyListStatus='Expiry', 'Expired',cgs.GreyListStatus) from GreyList gl, CodeTable_GreyListStatus cgs \
where gl.StatusID = cgs.GreyListStatusID and cgs.GreyListStatus != 'Deleted'; 
