select bl.BlackListID,bl.CreatedDate,IF(bl.Comments='',NULL,bl.Comments),IF(bl.CSStartDate='',NULL,bl.CSStartDate), \
       IF(bl.CSEndDate='',NULL,bl.CSEndDate),IF(bl.PIPStartDate='',NULL,bl.PIPStartDate),IF(bl.PIPEndDate='',NULL,bl.PIPEndDate), \
       IF(bl.PTStartDate='',NULL,bl.PTStartDate),IF(bl.PTEndDate='',NULL,bl.PTStartDate), \
       bl.Remarks,IF(bl.ResearchStartDate='',NULL,bl.ResearchStartDate),IF(bl.ResearchEndDate='',NULL,bl.ResearchEndDate), \
       IF(bl.IsDealCompleted='True','Expired','Active'),bl.CounterID from BlackoutList bl,CodeTable_Counter c  where bl.CounterID = c.CounterID and c.Active = 'True'; 
