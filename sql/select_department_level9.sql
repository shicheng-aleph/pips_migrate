select IFNULL(level9.DeptID,IFNULL(level8.DeptID,IFNULL(level7.DeptID,IFNULL(level6.DeptID,IFNULL(level5.DeptID,IFNULL(level4.DeptID,dept.DeptID)))))), \
       dept.DeptCode, \
       dept.Dept, \
       com.CountryID, \
       com.Company, \
       level4.DeptCode,level4.Dept, \
       level5.DeptCode,level5.Dept, \
       level6.DeptCode,level6.Dept, \
       level7.DeptCode,level7.Dept, \
       level8.DeptCode,level8.Dept, \
       level9.DeptCode,level9.Dept \
       from CodeTable_Dept as dept \
       left join CodeTable_Dept as level4 on level4.ParentDeptID= dept.DeptID  \
       left join CodeTable_Dept as level5 on level5.ParentDeptID= level4.DeptID  \
       left join CodeTable_Dept as level6 on level6.ParentDeptID= level5.DeptID  \
       left join CodeTable_Dept as level7 on level7.ParentDeptID= level6.DeptID  \
       left join CodeTable_Dept as level8 on level8.ParentDeptID= level7.DeptID  \
       left join CodeTable_Dept as level9 on level9.ParentDeptID= level8.DeptID  \
       left join CodeTable_Company as com on dept.CompanyID = com.CompanyID \
       where dept.ParentDeptID = 0 and level4.ParentDeptID != 0 and level5.ParentDeptID != 0 \
       and level6.ParentDeptID != 0 and level7.ParentDeptID != 0 and level8.ParentDeptID != 0 and level9.ParentDeptID != 0;
