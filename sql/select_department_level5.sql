select IFNULL(level5.DeptID,IFNULL(level4.DeptID,dept.DeptID)), \
       dept.DeptCode, \
       dept.Dept, \
       com.CountryID, \
       com.Company, \
       level4.DeptCode,level4.Dept, \
       level5.DeptCode,level5.Dept \
       from CodeTable_Dept as dept \
       left join CodeTable_Dept as level4 on level4.ParentDeptID= dept.DeptID  \
       left join CodeTable_Dept as level5 on level5.ParentDeptID= level4.DeptID  \
       left join CodeTable_Company as com on dept.CompanyID = com.CompanyID \
       where dept.ParentDeptID = 0 and level4.ParentDeptID != 0 and level5.ParentDeptID != 0;
