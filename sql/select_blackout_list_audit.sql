select bl.AID,bl.BlackListID,bl.CreatedDate,IF(bl.Comments='',NULL,bl.Comments),IF(bl.CSStartDate='',NULL,bl.CSStartDate), \
       IF(bl.CSEndDate='',NULL,bl.CSEndDate),IF(bl.PIPStartDate='',NULL,bl.PIPStartDate),IF(bl.PIPEndDate='',NULL,bl.PIPEndDate), \
       IF(bl.PTStartDate='',NULL,bl.PTStartDate),IF(bl.PTEndDate='',NULL,bl.PTStartDate), \
       bl.Remarks,IF(bl.ResearchStartDate='',NULL,bl.ResearchStartDate),IF(bl.ResearchEndDate='',NULL,bl.ResearchEndDate), \
       IF(bl.IsDealCompleted='True','Expired','Active'),bl.CounterID,(select count(AID) from A_BlackoutList where BlackListID = bl.BlackListID \
       and AID <= bl.AID) from A_BlackoutList bl, BlackoutList b, CodeTable_Counter c where bl.BlackListID = b.BlackListID and b.CounterID = c.CounterID \
       and c.Active = 'True';
