select dept.DeptID, \
       dept.DeptCode, \
       dept.Dept, \
       com.CountryID, \
       com.Company \
       from CodeTable_Dept as dept \
       left join CodeTable_Company as com on dept.CompanyID = com.CompanyID \
       where dept.ParentDeptID = 0;
